package org.jeecg.common.aspect.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented

/**
 * 默认是一分钟请求找过50次,限制等待300秒,默认值,可以自定义
 * @author Administrator
 *
 */

public @interface RequestLimit {
    int time() default 60;

    int count() default 50;

    int waits() default 300;

}
