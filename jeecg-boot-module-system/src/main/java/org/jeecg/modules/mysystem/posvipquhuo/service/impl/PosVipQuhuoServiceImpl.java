package org.jeecg.modules.mysystem.posvipquhuo.service.impl;

import org.jeecg.modules.mysystem.posvipquhuo.entity.PosVipQuhuo;
import org.jeecg.modules.mysystem.posvipquhuo.mapper.PosVipQuhuoMapper;
import org.jeecg.modules.mysystem.posvipquhuo.service.IPosVipQuhuoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 会员取货
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
@Service
public class PosVipQuhuoServiceImpl extends ServiceImpl<PosVipQuhuoMapper, PosVipQuhuo> implements IPosVipQuhuoService {

}
