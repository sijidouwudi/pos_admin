package org.jeecg.modules.mysystem.pospurchase.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.pospurchase.entity.PosPurchase;

/**
 * @Description: 商品采购
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
public interface IPosPurchaseService extends IService<PosPurchase> {

    IPage<PosPurchase> pages(Page<PosPurchase> page, PosPurchase queryWrapper);
}
