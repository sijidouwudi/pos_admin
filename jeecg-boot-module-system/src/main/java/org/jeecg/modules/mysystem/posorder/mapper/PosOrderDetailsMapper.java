package org.jeecg.modules.mysystem.posorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posorder.entity.PosOrderDetails;

/**
 * @Description: pos_order_details
 * @Author: jeecg-boot
 * @Date:   2021-04-29
 * @Version: V1.0
 */
public interface PosOrderDetailsMapper extends BaseMapper<PosOrderDetails> {

}
