package org.jeecg.modules.mysystem.posvipbalanceinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posvipbalanceinfo.entity.PosVipBalanceInfo;

/**
 * @Description: 会员卡消费记录
 * @Author: jeecg-boot
 * @Date:   2021-04-28
 * @Version: V1.0
 */
public interface PosVipBalanceInfoMapper extends BaseMapper<PosVipBalanceInfo> {

}
