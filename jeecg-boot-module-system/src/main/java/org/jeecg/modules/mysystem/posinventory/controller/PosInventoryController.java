package org.jeecg.modules.mysystem.posinventory.controller;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mysystem.posinventory.entity.PosInventory;
import org.jeecg.modules.mysystem.posinventory.mapper.PosInventoryMapper;
import org.jeecg.modules.mysystem.posinventory.service.IPosInventoryService;
import org.jeecg.modules.mysystem.posinventorygoods.mapper.PosInventoryGoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 库存盘点
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
@Api(tags="库存盘点")
@RestController
@RequestMapping("/posinventory/posInventory")
@Slf4j
public class PosInventoryController extends JeecgController<PosInventory, IPosInventoryService> {
	@Autowired
	private IPosInventoryService posInventoryService;

	@Autowired
	private PosInventoryMapper posInventoryMapper;

	@Autowired
	private PosInventoryGoodsMapper posInventoryGoodsMapper;
	
	/**
	 * 分页列表查询
	 *
	 * @param posInventory
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "库存盘点-分页列表查询")
	@ApiOperation(value="库存盘点-分页列表查询", notes="库存盘点-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PosInventory posInventory,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PosInventory> queryWrapper = QueryGenerator.initQueryWrapper(posInventory, req.getParameterMap());
		Page<PosInventory> page = new Page<PosInventory>(pageNo, pageSize);
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		queryWrapper.eq("sys_org_code",sysUser.getOrgCode());
		IPage<PosInventory> pageList = posInventoryService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param posInventory
	 * @return
	 */
	@AutoLog(value = "库存盘点-添加")
	@ApiOperation(value="库存盘点-添加", notes="库存盘点-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PosInventory posInventory) {
		//校验是否已经有存在正在盘点的订单
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		QueryWrapper queryWrapper=new QueryWrapper<PosInventory>(new PosInventory().setStatus("1").setSysOrgCode(sysUser.getOrgCode()));
		List list = posInventoryMapper.selectList(queryWrapper);
		if(CollUtil.isNotEmpty(list)){
			return Result.error("已存在正在盘点的订单");
		}
		posInventoryService.save(posInventory);
		posInventoryGoodsMapper.batchInsertGoods(sysUser.getOrgCode(),posInventory.getId());
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param posInventory
	 * @return
	 */
	@AutoLog(value = "库存盘点-编辑")
	@ApiOperation(value="库存盘点-编辑", notes="库存盘点-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PosInventory posInventory) throws Exception {
		posInventoryService.excuteService(posInventory);
		posInventoryService.updateById(posInventory);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "库存盘点-通过id删除")
	@ApiOperation(value="库存盘点-通过id删除", notes="库存盘点-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		posInventoryService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "库存盘点-批量删除")
	@ApiOperation(value="库存盘点-批量删除", notes="库存盘点-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.posInventoryService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "库存盘点-通过id查询")
	@ApiOperation(value="库存盘点-通过id查询", notes="库存盘点-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PosInventory posInventory = posInventoryService.getById(id);
		if(posInventory==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(posInventory);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param posInventory
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PosInventory posInventory) {
        return super.exportXls(request, posInventory, PosInventory.class, "库存盘点");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PosInventory.class);
    }

}
