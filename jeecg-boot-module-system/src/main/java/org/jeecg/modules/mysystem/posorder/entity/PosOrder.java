package org.jeecg.modules.mysystem.posorder.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 订单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-28
 * @Version: V1.0
 */
@Data
@TableName("pos_order")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="pos_order对象", description="订单主表")
public class PosOrder implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**小票流水*/
	@Excel(name = "小票流水", width = 15)
    @ApiModelProperty(value = "小票流水")
    private String code;
	/**vip号*/
	@Excel(name = "vip号", width = 15)
    @ApiModelProperty(value = "vip号")
    private String vipCard;
	/**总积分*/
	@Excel(name = "总积分", width = 15)
    @ApiModelProperty(value = "总积分")
    private BigDecimal points;
	/**支付类型*/
	@Excel(name = "支付类型", width = 15, dicCode = "pay_type")
	@Dict(dicCode = "pay_type")
    @ApiModelProperty(value = "支付类型")
    private String payType;
	/**商品数量*/
	@Excel(name = "商品数量", width = 15)
    @ApiModelProperty(value = "商品数量")
    private String counts;
	/**应收*/
	@Excel(name = "应收", width = 15)
    @ApiModelProperty(value = "应收")
    private BigDecimal receivableAmount;
	/**实收*/
	@Excel(name = "实收", width = 15)
    @ApiModelProperty(value = "实收")
    private BigDecimal actualAmount;

	/**优惠价*/
	@Excel(name = "优惠价", width = 15)
    @ApiModelProperty(value = "优惠价")
    private BigDecimal discounts;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "order_status")
	@Dict(dicCode = "order_status")
    @ApiModelProperty(value = "状态")
    private String status;

    /**补现*/
    @Excel(name = "补现", width = 15)
    @ApiModelProperty(value = "补现")
    private BigDecimal bxPrice;

    /**补现*/
    @Excel(name = "找零", width = 15)
    @ApiModelProperty(value = "找零")
    private BigDecimal zl;
}
