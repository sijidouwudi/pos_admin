package org.jeecg.modules.mysystem.posinventorygoods.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.api.client.util.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mysystem.posinventorygoods.entity.PosInventoryGoods;
import org.jeecg.modules.mysystem.posinventorygoods.service.IPosInventoryGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: 盘点商品
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
@Api(tags="盘点商品")
@RestController
@RequestMapping("/posinventorygoods/posInventoryGoods")
@Slf4j
public class PosInventoryGoodsController extends JeecgController<PosInventoryGoods, IPosInventoryGoodsService> {
	@Autowired
	private IPosInventoryGoodsService posInventoryGoodsService;
	
	/**
	 * 分页列表查询
	 *
	 * @param posInventoryGoods
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "盘点商品-分页列表查询")
	@ApiOperation(value="盘点商品-分页列表查询", notes="盘点商品-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PosInventoryGoods posInventoryGoods,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(StrUtil.isBlank(posInventoryGoods.getInventoryId())){
			return Result.OK(Lists.newArrayList());
		}
		QueryWrapper<PosInventoryGoods> queryWrapper = QueryGenerator.initQueryWrapper(posInventoryGoods, req.getParameterMap());
		Page<PosInventoryGoods> page = new Page<PosInventoryGoods>(pageNo, pageSize);
		IPage<PosInventoryGoods> pageList = posInventoryGoodsService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param posInventoryGoods
	 * @return
	 */
	@AutoLog(value = "盘点商品-添加")
	@ApiOperation(value="盘点商品-添加", notes="盘点商品-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PosInventoryGoods posInventoryGoods) {
		posInventoryGoodsService.save(posInventoryGoods);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param posInventoryGoods
	 * @return
	 */
	@AutoLog(value = "盘点商品-编辑")
	@ApiOperation(value="盘点商品-编辑", notes="盘点商品-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PosInventoryGoods posInventoryGoods) {
		posInventoryGoodsService.updateById(posInventoryGoods);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "盘点商品-通过id删除")
	@ApiOperation(value="盘点商品-通过id删除", notes="盘点商品-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		posInventoryGoodsService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "盘点商品-批量删除")
	@ApiOperation(value="盘点商品-批量删除", notes="盘点商品-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.posInventoryGoodsService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "盘点商品-通过id查询")
	@ApiOperation(value="盘点商品-通过id查询", notes="盘点商品-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PosInventoryGoods posInventoryGoods = posInventoryGoodsService.getById(id);
		if(posInventoryGoods==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(posInventoryGoods);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param posInventoryGoods
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PosInventoryGoods posInventoryGoods) {
        return super.exportXls(request, posInventoryGoods, PosInventoryGoods.class, "盘点商品");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PosInventoryGoods.class);
    }

}
