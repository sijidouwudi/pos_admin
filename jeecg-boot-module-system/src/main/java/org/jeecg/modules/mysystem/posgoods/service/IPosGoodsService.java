package org.jeecg.modules.mysystem.posgoods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.mysystem.posgoods.entity.PosGoods;
import org.jeecg.modules.mysystem.pospurchase.entity.PosPurchase;

/**
 * @Description: 商品表
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
public interface IPosGoodsService extends IService<PosGoods> {

    /**
     *  库存的添加和回退
     * @param posPurchase
     * @return void
     */
    void goodsCountsSaveProcess(PosPurchase posPurchase) throws JeecgBootException;

    /**
     * 查询订单
     * @param id
     * @return org.jeecg.common.api.vo.Result<?>
     */
    Result<?> executeService(String id);


    /**
     * @author: Mr.YSX
     * @description: pos前台积分兑换商品
     * @date: 2021/5/7 16:14
     * @param   vipCard  会员卡号
     * @param   goodId   商品id
     * @return
     */
    Result<?> pointDhGoods(String vipCard, String goodId,String number);
}
