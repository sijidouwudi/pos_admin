package org.jeecg.modules.mysystem.posorder.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mysystem.posorder.entity.PosOrder;
import org.jeecg.modules.mysystem.posorder.entity.VipUserBuyGoodsInfo;
import org.jeecg.modules.mysystem.posorder.vo.OrderVO;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 订单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-28
 * @Version: V1.0
 */
public interface IPosOrderService extends IService<PosOrder> {

    Result<?> submitOrder(OrderVO posOrder);

    Page<Map> queryPendingOrder(Page page, String orgCode);

    /**
     * @author: Mr.YSX
     * @description: 会员购买商品详情
     * @date: 2021/5/6 14:52
     * @param map
     * @return
     */
    IPage<VipUserBuyGoodsInfo> pages(Page<VipUserBuyGoodsInfo> page, Map<String, Object> map);

    Result<?> endPendingOrder(String id);

    Page<Map> queryRetractOrder(Page page, HashMap<String, String> map);

    Result<?> backOrder(String id, BigDecimal count);
}
