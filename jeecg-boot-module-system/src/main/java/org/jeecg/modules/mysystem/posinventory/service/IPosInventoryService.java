package org.jeecg.modules.mysystem.posinventory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.mysystem.posinventory.entity.PosInventory;

/**
 * @Description: 库存盘点
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
public interface IPosInventoryService extends IService<PosInventory> {

    void excuteService(PosInventory posInventory) throws JeecgBootException;
}
