package org.jeecg.modules.mysystem.posvipcunhuo.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mysystem.posvipcunhuo.entity.PosVipCunhuo;
import org.jeecg.modules.mysystem.posvipcunhuo.service.IPosVipCunhuoService;
import org.jeecg.modules.mysystem.posvipquhuo.entity.PosVipQuhuo;
import org.jeecg.modules.mysystem.posvipquhuo.service.IPosVipQuhuoService;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 会员存货
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
@Api(tags="会员存货")
@RestController
@RequestMapping("/posvipcunhuo/posVipCunhuo")
@Slf4j
public class PosVipCunhuoController extends JeecgController<PosVipCunhuo, IPosVipCunhuoService> {
	@Autowired
	private IPosVipCunhuoService posVipCunhuoService;

	@Autowired
	private IPosVipQuhuoService posVipQuhuoService;
	
	/**
	 * 分页列表查询
	 *
	 * @param posVipCunhuo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "会员存货-分页列表查询")
	@ApiOperation(value="会员存货-分页列表查询", notes="会员存货-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PosVipCunhuo posVipCunhuo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PosVipCunhuo> queryWrapper = QueryGenerator.initQueryWrapper(posVipCunhuo, req.getParameterMap());
		Page<PosVipCunhuo> page = new Page<PosVipCunhuo>(pageNo, pageSize);
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		queryWrapper.eq("sys_org_code",sysUser.getOrgCode());
		queryWrapper.eq("delete_flag",0);
		IPage<PosVipCunhuo> pageList = posVipCunhuoService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param posVipCunhuo
	 * @return
	 */
	@AutoLog(value = "会员存货-添加")
	@ApiOperation(value="会员存货-添加", notes="会员存货-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PosVipCunhuo posVipCunhuo) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		posVipCunhuo.setSysOrgCode(sysUser.getOrgCode());
		posVipCunhuo.setDeleteFlag(0);
		posVipCunhuoService.save(posVipCunhuo);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param posVipCunhuo
	 * @return
	 */
	@AutoLog(value = "会员存货-编辑")
	@ApiOperation(value="会员存货-编辑", notes="会员存货-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PosVipCunhuo posVipCunhuo) {
		posVipCunhuoService.updateById(posVipCunhuo);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员存货-通过id删除")
	@ApiOperation(value="会员存货-通过id删除", notes="会员存货-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (ObjectUtils.isEmpty(this.posVipCunhuoService.getById(id))) {
			return Result.error("删除失败, 请刷新后再试!");
		}
		boolean b = this.posVipCunhuoService.updateById(new PosVipCunhuo().setId(id).setDeleteFlag(1).setUpdateTime(new Date()).setUpdateBy(sysUser.getOrgCode()));
		if (b){
			return Result.ok("删除成功!");
		}
		throw new JeecgBootException("删除失败, 请刷新后再试!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "会员存货-批量删除")
	@ApiOperation(value="会员存货-批量删除", notes="会员存货-批量删除")
//	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.posVipCunhuoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员存货-通过id查询")
	@ApiOperation(value="会员存货-通过id查询", notes="会员存货-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PosVipCunhuo posVipCunhuo = posVipCunhuoService.getById(id);
		if(posVipCunhuo==null) {
			return Result.error("未找到对应数据");
		}
		if (posVipCunhuo.getDeleteFlag().intValue() == 1) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(posVipCunhuo);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param posVipCunhuo
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PosVipCunhuo posVipCunhuo) {
        return super.exportXls(request, posVipCunhuo, PosVipCunhuo.class, "会员存货");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PosVipCunhuo.class);
    }

    /**
     * @author: Mr.YSX
     * @description: 会员存货总金额
     * @date: 2021/4/23 11:23
     * @return  /posvipcunhuo/posVipCunhuo/amountCount
     */
	@RequestMapping(value = "/amountCount", method = RequestMethod.POST)
    public Result<?> amountCount() {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		double count = this.posVipCunhuoService.amountCount(sysUser.getOrgCode());
		return Result.ok(count);
	}


	 /**
	  * @author: Mr.YSX
	  * @description: 会员取货
	  * @date: 2021/4/23 14:26
	  * @param id  count
	  * @return   /posvipcunhuo/posVipCunhuo/vipQuHuo
	  */
	 @Transactional
	 @RequestMapping(value = "/vipQuHuo", method = RequestMethod.POST)
	 public Result<?> vupQuHuo(@RequestBody JSONObject jsonObject) {
		 if (StringUtils.isEmpty(jsonObject.get("id").toString())) {
			 return Result.error("参数错误!");
		 }
		 if (StringUtils.isEmpty(jsonObject.get("count").toString())) {
			 return Result.error("参数错误!");
		 }
		 int count = Integer.parseInt(jsonObject.get("count").toString());
		 PosVipCunhuo posVipCunhuoServiceById = this.posVipCunhuoService.getById(jsonObject.get("id").toString());
		 if(posVipCunhuoServiceById.getGoodCount()<count){
			 return Result.error("取货数量大于存货数量");
		 }
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 //取货
		 PosVipQuhuo posVipQuhuo = new PosVipQuhuo();
		 posVipQuhuo.setCunId(posVipCunhuoServiceById.getId());
		 posVipQuhuo.setCreateBy(sysUser.getOrgCode());
		 posVipQuhuo.setDeleteFlag(0);
		 posVipQuhuo.setQuCount(count);
		 boolean save = this.posVipQuhuoService.save(posVipQuhuo);
		 if (!save){
		 	throw new JeecgBootException("取货失败!");
		 }
		 //修改存货信息
		 int updateCount = posVipCunhuoServiceById.getGoodCount() - count;
		 PosVipCunhuo posVipCunhuo = new PosVipCunhuo();
		 posVipCunhuo.setId(posVipCunhuoServiceById.getId());
		 posVipCunhuo.setGoodCount(updateCount);
		 posVipCunhuo.setUpdateBy(sysUser.getOrgCode());
		 boolean b = this.posVipCunhuoService.updateById(posVipCunhuo);
		 if (!b){
			 throw new JeecgBootException("取货失败!");
		 }
		 return Result.ok("取货成功!");
	 }


	 /**
	  * @author: Mr.YSX
	  * @description: 会员取货记录
	  * @date: 2021/4/23 17:37
	  * @param id
	  * @return  /posvipcunhuo/posVipCunhuo/vipQuHuoInfo
	  */
	 @RequestMapping(value = "/vipQuHuoInfo", method = RequestMethod.POST)
	 public Result<?> vipQuHuoInfo(@RequestBody JSONObject jsonObject){
		 if (StrUtil.isBlank(jsonObject.get("id").toString())) {
			 return Result.error("参数为空!");
		 }
		 String id = jsonObject.get("id").toString();
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 QueryWrapper<PosVipQuhuo> posVipQuhuoQueryWrapper = new QueryWrapper<>();
		 posVipQuhuoQueryWrapper.eq("cun_id",id);
		 posVipQuhuoQueryWrapper.eq("sys_org_code",sysUser.getOrgCode());
		 posVipQuhuoQueryWrapper.eq("delete_flag",0);
		 List<PosVipQuhuo> list = this.posVipQuhuoService.list(posVipQuhuoQueryWrapper);
		 if (CollectionUtils.isEmpty(list)) {
			 return Result.ok("暂时还没有取货记录!");
		 }

		 return Result.ok(list);
	 }


}
