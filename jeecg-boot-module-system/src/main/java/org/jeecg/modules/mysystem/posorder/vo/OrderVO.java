package org.jeecg.modules.mysystem.posorder.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author yuanxin
 * @Date 2021/4/29 15:57
 **/
@Data
public class OrderVO {

    //总数
    private String counts;
    //实收
    private BigDecimal actualAmount;
    //应收
    private BigDecimal receivableAmount;
    //积分
    private BigDecimal points;

    @NotBlank(message = "订单号不能为空")
    private String code;
    //会员卡号
    private String vipCard;
    //支付类型
    private String payType;
    //订单状态
    private String status;

    private BigDecimal bxPrice;

    private BigDecimal discounts;

    private BigDecimal zl;

    private List<OrderDetailsVO> list;


}