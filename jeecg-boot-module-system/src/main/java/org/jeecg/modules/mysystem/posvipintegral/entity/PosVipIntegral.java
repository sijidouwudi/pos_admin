package org.jeecg.modules.mysystem.posvipintegral.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 会员积分详情
 * @Author: jeecg-boot
 * @Date:   2021-04-24
 * @Version: V1.0
 */
@Data
@TableName("pos_vip_integral")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="pos_vip_integral对象", description="会员积分详情")
public class PosVipIntegral implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**变化数量*/
	@Excel(name = "变化数量", width = 15)
    @ApiModelProperty(value = "变化数量")
    private BigDecimal integralCount;
	/**变化方式*/
	@Excel(name = "变化方式", width = 15, dicCode = "way")
	@Dict(dicCode = "way")
    @ApiModelProperty(value = "变化方式")
    private String type;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**会员卡号*/
	@Excel(name = "会员卡号", width = 15)
    @ApiModelProperty(value = "会员卡号")
    private String vipNumberI;
	/**会员姓名*/
	@Excel(name = "会员姓名", width = 15)
    @ApiModelProperty(value = "会员姓名")
    private String vipNameI;
	/**会员生日*/
	@Excel(name = "会员生日", width = 15)
    @ApiModelProperty(value = "会员生日")
    private String vipBir;
	/**会员性别*/
	@Excel(name = "会员性别", width = 15)
    @ApiModelProperty(value = "会员性别")
    private String vipSexI;
	/**会员手机号*/
	@Excel(name = "会员手机号", width = 15)
    @ApiModelProperty(value = "会员手机号")
    private String vipPhoneI;
	/**会员积分*/
	@Excel(name = "会员积分", width = 15)
    @ApiModelProperty(value = "会员积分")
    private String vipIntegralI;
}
