package org.jeecg.modules.mysystem.posvipuser.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 会员信息后台查询使用
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PosVipUserVO implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**会员卡号*/
	@Excel(name = "会员卡号", width = 15)
    @ApiModelProperty(value = "会员卡号")
    private String vipNumber;
	/**会员姓名*/
	@Excel(name = "会员姓名", width = 15)
    @ApiModelProperty(value = "会员姓名")
    private String vipName;
	/**手机号*/
	@Excel(name = "手机号", width = 15)
    @ApiModelProperty(value = "手机号")
    private String vipPhone;
	/**会员生日*/
	@Excel(name = "会员生日", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "会员生日")
    private Date vipBirthday;
	/**会员标签*/
    @Excel(name = "会员标签", width = 15, dictTable = "pos_vip_mark", dicText = "mark_name", dicCode = "id")
    @Dict(dictTable = "pos_vip_mark", dicText = "mark_name", dicCode = "id")
    @ApiModelProperty(value = "会员标签")
    private String vipTag;
	/**会员地址*/
	@Excel(name = "会员地址", width = 15)
    @ApiModelProperty(value = "会员地址")
    private String vipAddress;
	/**会员性别*/
	@Excel(name = "会员性别", width = 15, dicCode = "sex")
	@Dict(dicCode = "sex")
    @ApiModelProperty(value = "会员性别")
    private String vipSex;
	/**会员余额*/
	@Excel(name = "会员余额", width = 15)
    @ApiModelProperty(value = "会员余额")
    private BigDecimal vipBlance;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String vipRemark;
	/**删除标记0删除1未删除*/
	@Excel(name = "删除标记0删除1未删除", width = 15)
    @ApiModelProperty(value = "删除标记0删除1未删除")
    private Integer deleteFlag;
	/**是否禁用*/
	@Excel(name = "是否禁用", width = 15, dicCode = "disabled")
	@Dict(dicCode = "disabled")
    @ApiModelProperty(value = "是否禁用")
    private Integer vipDisable;

    /**会员积分*/
    @Excel(name = "会员积分", width = 15)
    @ApiModelProperty(value = "会员积分")
    private Integer vipIntegral;
}
