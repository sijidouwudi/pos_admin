package org.jeecg.modules.mysystem.possupplier.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mysystem.possupplier.entity.PosSupplier;
import org.jeecg.modules.mysystem.possupplier.mapper.PosSupplierMapper;
import org.jeecg.modules.mysystem.possupplier.service.IPosSupplierService;
import org.springframework.stereotype.Service;

/**
 * @Description: 供应商
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
@Service
public class PosSupplierServiceImpl extends ServiceImpl<PosSupplierMapper, PosSupplier> implements IPosSupplierService {

}
