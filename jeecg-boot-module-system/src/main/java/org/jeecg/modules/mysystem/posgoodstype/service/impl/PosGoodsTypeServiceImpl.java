package org.jeecg.modules.mysystem.posgoodstype.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mysystem.posgoodstype.entity.PosGoodsType;
import org.jeecg.modules.mysystem.posgoodstype.mapper.PosGoodsTypeMapper;
import org.jeecg.modules.mysystem.posgoodstype.service.IPosGoodsTypeService;
import org.springframework.stereotype.Service;

/**
 * @Description: 商品分类
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
@Service
public class PosGoodsTypeServiceImpl extends ServiceImpl<PosGoodsTypeMapper, PosGoodsType> implements IPosGoodsTypeService {

}
