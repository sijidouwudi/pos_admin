package org.jeecg.modules.mysystem.posvipcunhuo.service.impl;

import org.jeecg.modules.mysystem.posvipcunhuo.entity.PosVipCunhuo;
import org.jeecg.modules.mysystem.posvipcunhuo.mapper.PosVipCunhuoMapper;
import org.jeecg.modules.mysystem.posvipcunhuo.service.IPosVipCunhuoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 会员存货
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
@Service
public class PosVipCunhuoServiceImpl extends ServiceImpl<PosVipCunhuoMapper, PosVipCunhuo> implements IPosVipCunhuoService {


    /**
     * @author: Mr.YSX
     * @description: 会员存货总金额
     * @date: 2021/4/23 14:14
     * @return
     */
    @Override
    public double amountCount(String OrgCode) {
        return this.baseMapper.amountCount(OrgCode);
    }
}
