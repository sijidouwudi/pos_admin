package org.jeecg.modules.mysystem.couser.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;

import org.jeecg.modules.mysystem.couser.entity.CoUser;
import org.jeecg.modules.mysystem.couser.service.ICoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * @Description: 通用登录代码
 * @Author: jeecg-boot
 * @Date:   2020-09-02
 * @Version: V1.0
 */
@Api(tags="通用登录代码")
@RestController
@RequestMapping("/couser/coUser")
@Slf4j
public class CoUserController extends JeecgController<CoUser, ICoUserService> {
	@Autowired
	private ICoUserService coUserService;
	
	/**
	 * 分页列表查询  11 1
	 *
	 * @param coUser
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "通用登录代码-分页列表查询")
	@ApiOperation(value="通用登录代码-分页列表查询", notes="通用登录代码-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CoUser coUser,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CoUser> queryWrapper = QueryGenerator.initQueryWrapper(coUser, req.getParameterMap());
		Page<CoUser> page = new Page<CoUser>(pageNo, pageSize);
		IPage<CoUser> pageList = coUserService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param coUser
	 * @return
	 */
	@AutoLog(value = "通用登录代码-添加")
	@ApiOperation(value="通用登录代码-添加", notes="通用登录代码-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CoUser coUser) {
		coUserService.save(coUser);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param coUser
	 * @return
	 */
	@AutoLog(value = "通用登录代码-编辑")
	@ApiOperation(value="通用登录代码-编辑", notes="通用登录代码-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CoUser coUser) {
		coUserService.updateById(coUser);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "通用登录代码-通过id删除")
	@ApiOperation(value="通用登录代码-通过id删除", notes="通用登录代码-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		coUserService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "通用登录代码-批量删除")
	@ApiOperation(value="通用登录代码-批量删除", notes="通用登录代码-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.coUserService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "通用登录代码-通过id查询")
	@ApiOperation(value="通用登录代码-通过id查询", notes="通用登录代码-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CoUser coUser = coUserService.getById(id);
		if(coUser==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(coUser);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param coUser
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CoUser coUser) {
        return super.exportXls(request, coUser, CoUser.class, "通用登录代码");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CoUser.class);
    }

	 @RequestMapping(value = "/freezeTheAccount", method = RequestMethod.PUT)
	 public Result<?> freezeTheAccount(@RequestBody CoUser coUser) {
		 return coUserService.freezeTheAccount(coUser);
	 }
}
