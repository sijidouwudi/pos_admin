package org.jeecg.modules.mysystem.posvipuser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mysystem.posviptopup.entity.PosVipTopUp;
import org.jeecg.modules.mysystem.posviptopup.service.IPosVipTopUpService;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUser;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUserVO;
import org.jeecg.modules.mysystem.posvipuser.mapper.PosVipUserMapper;
import org.jeecg.modules.mysystem.posvipuser.service.IPosVipUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 会员信息
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
@Service
public class PosVipUserServiceImpl extends ServiceImpl<PosVipUserMapper, PosVipUser> implements IPosVipUserService {

    @Autowired
    private IPosVipTopUpService posVipTopUpService;

    /**
     * @author: Mr.YSX
     * @description: 查询会员总余额
     * @date: 2021/4/22 17:50
     * @return
     */
    @Override
    public double balanceCount(String orgCode) {
        return this.baseMapper.balanceCount(orgCode);
    }


    /**
     * @author: Mr.YSX
     * @description: 会员信息-分页列表查询
     * @date: 2021/4/24 10:46
     * @return
     */
    @Override
    public IPage<PosVipUserVO> pages(Page<PosVipUserVO> page, Map<String, Object> map) {
        return this.baseMapper.selectPages(page,map);
    }


    /**
     * @author: Mr.YSX
     * @description: 会员卡充值
     * @date: 2021/4/27 19:00
     * @param vipNumber  money
     * @return
     */
    @Transactional
    @Override
    public Result<?> vipTopUp(HashMap<String, Object> map, BigDecimal money) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //通过会员卡查询会员信息
//        QueryWrapper<PosVipUser> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("vip_number",vipNumber);
//        queryWrapper.eq("delete_flag",0);
        PosVipUser posVipUser = this.baseMapper.queryVipInfoByVipNumber(map);
        if (ObjectUtils.isEmpty(posVipUser)) {
            return Result.error("该会员不存在或已被删除!");
        }
        BigDecimal vipBlance = posVipUser.getVipBlance();  // 会员卡余额
        BigDecimal add = vipBlance.add(money);
        posVipUser.setVipBlance(add);
        int updateResult = this.baseMapper.updateById(posVipUser);
        if (updateResult == 1){
            //创建充值记录
            boolean save = this.posVipTopUpService.save(new PosVipTopUp()
                    .setCreateBy(sysUser.getUsername())
                    .setSysOrgCode(sysUser.getOrgCode())
                    .setMoney(money.toString())
                    .setCreateTime(new Date())
                    .setVipNameTop(posVipUser.getId()));
            if (save) {
                return Result.ok("充值成功!");
            }
            throw new JeecgBootException("充值失败,请稍后再试!");
        }
        throw new JeecgBootException("充值失败,请稍后再试!");
    }

    @Override
    public PosVipUser queryVipInfoByVipNumber(HashMap<String, Object> map) {
        return this.baseMapper.queryVipInfoByVipNumber(map);
    }
}
