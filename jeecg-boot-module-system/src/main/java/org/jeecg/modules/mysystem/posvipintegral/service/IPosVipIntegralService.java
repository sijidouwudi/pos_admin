package org.jeecg.modules.mysystem.posvipintegral.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posvipintegral.entity.PosVipIntegral;

import java.math.BigDecimal;

/**
 * @Description: 会员积分详情
 * @Author: jeecg-boot
 * @Date:   2021-04-24
 * @Version: V1.0
 */
public interface IPosVipIntegralService extends IService<PosVipIntegral> {

    /**
     * @author: Mr.YSX
     * @description: 结账增加用户积分消费信息
     * @date: 2021/5/5 16:42
     * @param integralCount 变化数量
     * @param type          变化方式   字符串 1 加     2 减     3 无
     * @param userId        会员id
     * @return
     */
    boolean saveIntegralInfo(BigDecimal integralCount, String type, String userId);

    /**
     * @author: Mr.YSX
     * @description: 结账增加用户积分消费信息
     * @date: 2021/5/7 16:55
     * @param integralCount 变化数量
     * @param type          变化方式   字符串 1 加     2 减     3 无
     * @param userId        会员id
     * @param remark        备注
     * @return
     */
    boolean saveIntegralInfo(BigDecimal integralCount, String type, String userId, String remark);

}
