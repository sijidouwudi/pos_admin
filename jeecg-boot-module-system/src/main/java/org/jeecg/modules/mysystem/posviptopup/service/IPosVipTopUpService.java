package org.jeecg.modules.mysystem.posviptopup.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posviptopup.entity.PosVipTopUp;

import java.util.Map;

/**
 * @Description: 会员充值
 * @Author: jeecg-boot
 * @Date:   2021-04-26
 * @Version: V1.0
 */
public interface IPosVipTopUpService extends IService<PosVipTopUp> {

    /**
     * @author: Mr.YSX
     * @description: 会员充值-分页列表查询
     * @date: 2021/4/26 16:20
     * @param page map
     * @return
     */
    IPage<PosVipTopUp> pages(Page<PosVipTopUp> page, Map<String, Object> map);
}
