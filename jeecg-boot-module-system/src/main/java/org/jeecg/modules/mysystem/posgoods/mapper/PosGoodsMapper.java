package org.jeecg.modules.mysystem.posgoods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mysystem.posgoods.entity.PosGoods;
import org.jeecg.modules.mysystem.posinventorygoods.entity.PosInventoryGoods;
import org.jeecg.modules.mysystem.posorder.vo.OrderDetailsVO;

import java.util.HashMap;
import java.util.List;

/**
 * @Description: 商品表
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
public interface PosGoodsMapper extends BaseMapper<PosGoods> {


    /**
     * 增加商品库存
     * @param updateData
     * @return int
     */
    int updateGoodsCount(@Param("params") HashMap<String, Object> updateData);

    /**
     *  回退商品库存
     * @param posPurchase
     * @return int
     */
    int backGoodsCounts(@Param("params") HashMap<String, Object> updateData);

    /**
     * 库存盘点修改库存
     * @param posInventoryGoods
     * @return void
     */
    int batchUpdstes(@Param("list") List<PosInventoryGoods> posInventoryGoods);


    int updateBatch(@Param("list")List<OrderDetailsVO> list);
}
