package org.jeecg.modules.mysystem.posorder.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.api.client.util.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mysystem.posgoods.entity.PosGoods;
import org.jeecg.modules.mysystem.posgoods.mapper.PosGoodsMapper;
import org.jeecg.modules.mysystem.posinventory.entity.PosInventory;
import org.jeecg.modules.mysystem.posinventory.mapper.PosInventoryMapper;
import org.jeecg.modules.mysystem.posorder.entity.PosOrder;
import org.jeecg.modules.mysystem.posorder.entity.PosOrderBack;
import org.jeecg.modules.mysystem.posorder.entity.PosOrderDetails;
import org.jeecg.modules.mysystem.posorder.entity.VipUserBuyGoodsInfo;
import org.jeecg.modules.mysystem.posorder.mapper.PosOrderBackMapper;
import org.jeecg.modules.mysystem.posorder.mapper.PosOrderDetailsMapper;
import org.jeecg.modules.mysystem.posorder.mapper.PosOrderMapper;
import org.jeecg.modules.mysystem.posorder.service.IPosOrderDetailsService;
import org.jeecg.modules.mysystem.posorder.service.IPosOrderService;
import org.jeecg.modules.mysystem.posorder.vo.OrderDetailsVO;
import org.jeecg.modules.mysystem.posorder.vo.OrderVO;
import org.jeecg.modules.mysystem.posvipbalanceinfo.service.IPosVipBalanceInfoService;
import org.jeecg.modules.mysystem.posvipintegral.service.IPosVipIntegralService;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUser;
import org.jeecg.modules.mysystem.posvipuser.mapper.PosVipUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: 订单主表
 * @Author: jeecg-boot
 * @Date: 2021-04-28
 * @Version: V1.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class PosOrderServiceImpl extends ServiceImpl<PosOrderMapper, PosOrder> implements IPosOrderService {
    @Autowired
    private PosInventoryMapper posInventoryMapper;

    @Autowired
    private IPosOrderDetailsService iPosOrderDetailsService;

    @Autowired
    private PosGoodsMapper posGoodsMapper;

    @Autowired
    private PosVipUserMapper posVipUserMapper;

    @Autowired
    private IPosVipIntegralService iPosVipIntegralService;

    @Autowired
    private PosOrderDetailsMapper posOrderDetailsMapper;

    @Autowired
    private PosOrderBackMapper posOrderBackMapper;

    @Autowired
    private IPosVipBalanceInfoService iPosVipBalanceInfoService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result<?> submitOrder(OrderVO posOrder) {
        if ("3".equals(posOrder.getStatus())) {
            return guadan(posOrder);
        }
        //限制门店处于盘点状态下不能使用结算功能
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper queryWrapper = new QueryWrapper<PosInventory>(new PosInventory().setStatus("1").setSysOrgCode(sysUser.getOrgCode()));
        List list = posInventoryMapper.selectList(queryWrapper);
        if (CollUtil.isNotEmpty(list)) {
            return Result.error("后台存在盘点不能使用结算功能");
        }
        PosOrder posOrder1 = new PosOrder();
        posOrder1 = baseMapper.selectOne(new QueryWrapper<PosOrder>().eq("code", posOrder.getCode()).eq("sys_org_code", sysUser.getOrgCode()));
        if (posOrder1 == null) {
            throw new JeecgBootException("订单异常,请联系管理员");
        }
        BeanUtil.copyProperties(posOrder, posOrder1);
        baseMapper.updateById(posOrder1);
        List<PosOrderDetails> listpos = Lists.newArrayList();
        for (OrderDetailsVO orderDetailsVO : posOrder.getList()) {
            PosOrderDetails posOrderDetails = new PosOrderDetails();
            BeanUtil.copyProperties(orderDetailsVO, posOrderDetails);
            posOrderDetails.setOrderId(posOrder1.getId());
            listpos.add(posOrderDetails);
        }
        iPosOrderDetailsService.saveBatch(listpos);
        //修改商品库存
        int count = posGoodsMapper.updateBatch(posOrder.getList());
        if (count != posOrder.getList().size()) {
            throw new JeecgBootException("操作频繁,请稍后重试");
        }
        if (StrUtil.isNotBlank(posOrder.getVipCard())) {
            PosVipUser posVipUsers = posVipUserMapper.selectOne(new QueryWrapper<PosVipUser>(new PosVipUser().setVipNumber(posOrder.getVipCard()).setSysOrgCode(sysUser.getOrgCode()).setDeleteFlag(0)));
            if (posVipUsers == null) {
                throw new JeecgBootException("找不到会员信息");
            }
            //如果是会员卡支付
            if ("5".equals(posOrder.getPayType())) {
                boolean falg = true;
                if (posOrder.getReceivableAmount().compareTo(posVipUsers.getVipBlance()) == 1) {
                    falg = iPosVipBalanceInfoService.saveBalancelInfo(1, posVipUsers.getVipBlance(), new BigDecimal(0), posOrder.getReceivableAmount(), "2", posVipUsers.getId());
                    posVipUsers.setVipBlance(new BigDecimal(0));
                } else {
                    falg = iPosVipBalanceInfoService.saveBalancelInfo(1, posVipUsers.getVipBlance(), posVipUsers.getVipBlance().subtract(posOrder.getReceivableAmount()), posOrder.getReceivableAmount(), "2", posVipUsers.getId());
                    posVipUsers.setVipBlance(posVipUsers.getVipBlance().subtract(posOrder.getReceivableAmount()));

                }
                if (!falg) {
                    throw new JeecgBootException("世贤接口报错");
                }
            }

            iPosVipIntegralService.saveIntegralInfo(posOrder.getPoints(), "1", posVipUsers.getId());
            posVipUsers.setVipIntegral(posVipUsers.getVipIntegral().add(posOrder.getPoints()));
            posVipUserMapper.updateById(posVipUsers);
        }
        return Result.ok("订单结算成功");
    }

    @Override
    public Page<Map> queryPendingOrder(Page page, String orgCode) {
        Page<Map> data = this.baseMapper.queryPendingOrder(page, orgCode);
        List<Map> records = data.getRecords();
        ArrayList<Map> objects = Lists.newArrayList();
        if (CollUtil.isNotEmpty(records)) {
            Map<String, List<Map>> id = records.stream().collect(Collectors.groupingBy(map -> map.get("id").toString()));
            for (String s : id.keySet()) {
                HashMap<Object, Object> objectObjectHashMap = Maps.newHashMap();
                List<Map> maps = id.get(s);
                StringBuffer strName = new StringBuffer();
                for (Map map : maps) {
                    strName.append(map.get("goods_name").toString() + "/");
                    objectObjectHashMap.put("createBy", map.get("create_by"));
                    objectObjectHashMap.put("createTime", map.get("create_time"));
                    objectObjectHashMap.put("id", map.get("id"));
                    objectObjectHashMap.put("code", map.get("code"));
                }
                String goodsName = StrUtil.removeSuffix(strName, "/");
                objectObjectHashMap.put("goodsName", goodsName);
                objects.add(objectObjectHashMap);
            }
        }
        data.setRecords(objects);
        return data;
    }

    @Override
    public Result<?> endPendingOrder(String id) {
        //查询挂单数据
        List<Map<String, Object>> maps = this.baseMapper.endPendingOrder(id);
        if (CollUtil.isNotEmpty(maps)) {
            //删除挂单数据
            baseMapper.deleteOrderAndDetails(id);
            //更改订单状态
            baseMapper.updateOrder(id);
            //保存当前订单
            return Result.ok(maps);
        }
        return Result.error("查无次挂单");
    }

    @Override
    public Page<Map> queryRetractOrder(Page page, HashMap<String, String> mapdata) {
        Page<Map> data = this.baseMapper.queryRetractOrder(page, mapdata);
        return data;
    }

    @Override
    public Result<?> backOrder(String id, BigDecimal count) {
        PosOrderDetails posOrderDetails = posOrderDetailsMapper.selectById(id);
        PosGoods posGoods = posGoodsMapper.selectById(posOrderDetails.getGoodsId());
        //更新商品库存
        HashMap<String, Object> objectObjectHashMap = Maps.newHashMap();
        objectObjectHashMap.put("id", posGoods.getId());
        objectObjectHashMap.put("version", posGoods.getVersion());
        objectObjectHashMap.put("count", count);
        int i = posGoodsMapper.updateGoodsCount(objectObjectHashMap);
        if (i == 0) {
            throw new JeecgBootException("用人正在修改此商品的库存,请稍后重试");
        }
        PosOrder posOrder = baseMapper.selectById(posOrderDetails.getOrderId());
        if (StrUtil.isNotBlank(posOrder.getVipCard())) {
            LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
            //如果此订单有会员卡号的话 扣除积分
            BigDecimal points = count.multiply(posGoods.getPoints()).setScale(2, RoundingMode.HALF_UP);
            PosVipUser posVipUsers = posVipUserMapper.selectOne(new QueryWrapper<PosVipUser>(new PosVipUser().setVipNumber(posOrder.getVipCard()).setSysOrgCode(sysUser.getOrgCode()).setDeleteFlag(0)));
            if (posVipUsers == null) {
                throw new JeecgBootException("找不到会员信息");
            }
            posVipUsers.setVipIntegral(posVipUsers.getVipIntegral().subtract(points));
            iPosVipIntegralService.saveIntegralInfo(points, "2", posVipUsers.getId());
            posVipUserMapper.updateById(posVipUsers);
        }
        //加库存
        PosOrderBack posOrderBack = new PosOrderBack();
        posOrderBack.setOrderDetailsId(id);
        posOrderBack.setGoodsCount(count);
        posOrderBack.setPrice(count.multiply(new BigDecimal(posOrderDetails.getPrice())));
        posOrderBackMapper.insert(posOrderBack);
        return Result.ok("退货成功");
    }

    private Result<?> guadan(OrderVO posOrder) {
//限制门店处于盘点状态下不能使用结算功能
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper queryWrapper = new QueryWrapper<PosInventory>(new PosInventory().setStatus("1").setSysOrgCode(sysUser.getOrgCode()));
        List list = posInventoryMapper.selectList(queryWrapper);
        if (CollUtil.isNotEmpty(list)) {
            return Result.error("后台存在盘点不能使用结算功能");
        }
        PosOrder posOrder1 = new PosOrder();
        posOrder1 = baseMapper.selectOne(new QueryWrapper<PosOrder>().eq("code", posOrder.getCode()).eq("sys_org_code", sysUser.getOrgCode()));
        if (posOrder1 == null) {
            throw new JeecgBootException("订单异常,请联系管理员");
        }
        BeanUtil.copyProperties(posOrder, posOrder1);
        baseMapper.updateById(posOrder1);
        List<PosOrderDetails> listpos = Lists.newArrayList();
        for (OrderDetailsVO orderDetailsVO : posOrder.getList()) {
            PosOrderDetails posOrderDetails = new PosOrderDetails();
            BeanUtil.copyProperties(orderDetailsVO, posOrderDetails);
            posOrderDetails.setOrderId(posOrder1.getId());
            listpos.add(posOrderDetails);
        }
        iPosOrderDetailsService.saveBatch(listpos);
        return Result.ok("挂单成功");
    }

    /**
     * @param map
     * @return
     * @author: Mr.YSX
     * @description: 会员购买商品详情
     * @date: 2021/5/6 14:53
     */
    @Override
    public IPage<VipUserBuyGoodsInfo> pages(Page<VipUserBuyGoodsInfo> page, Map<String, Object> map) {
        return this.baseMapper.selectPages(page, map);
    }
}
