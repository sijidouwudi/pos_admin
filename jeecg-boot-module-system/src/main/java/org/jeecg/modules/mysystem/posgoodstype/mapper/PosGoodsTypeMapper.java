package org.jeecg.modules.mysystem.posgoodstype.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posgoodstype.entity.PosGoodsType;

/**
 * @Description: 商品分类
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
public interface PosGoodsTypeMapper extends BaseMapper<PosGoodsType> {

}
