package org.jeecg.modules.mysystem.posgoodstype.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posgoodstype.entity.PosGoodsType;

/**
 * @Description: 商品分类
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
public interface IPosGoodsTypeService extends IService<PosGoodsType> {

}
