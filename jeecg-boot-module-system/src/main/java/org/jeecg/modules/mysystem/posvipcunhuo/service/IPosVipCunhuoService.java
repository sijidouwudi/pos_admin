package org.jeecg.modules.mysystem.posvipcunhuo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posvipcunhuo.entity.PosVipCunhuo;

/**
 * @Description: 会员存货
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
public interface IPosVipCunhuoService extends IService<PosVipCunhuo> {

    /**
     * @author: Mr.YSX
     * @description: 会员存货总金额
     * @date: 2021/4/23 14:14
     * @return
     */
    double amountCount(String OrgCode);
}
