package org.jeecg.modules.mysystem.couser.service.impl;


import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.util.RedisUtil;

import org.jeecg.modules.mysystem.couser.entity.CoUser;
import org.jeecg.modules.mysystem.couser.mapper.CoUserMapper;
import org.jeecg.modules.mysystem.couser.service.ICoUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Set;

/**
 * @Description: 通用登录代码
 * @Author: jeecg-boot
 * @Date: 2020-09-02
 * @Version: V1.0
 */
@Service
public class CoUserServiceImpl extends ServiceImpl<CoUserMapper, CoUser> implements ICoUserService {

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public Result<?> freezeTheAccount(CoUser map) {
        int i = baseMapper.updateById(map);
        //把Token变失效
        Set keys = redisUtil.keys(CommonConstant.PREFIX_USER_APP_TOKEN + map.getNumber());
        if(CollUtil.isNotEmpty(keys)){
            Iterator<String> it = keys.iterator();
            while (it.hasNext()) {
                redisUtil.del(it.next());
            }
        }
        if (i == 1) {
            return Result.ok(map.getStatus() == 1 ? "解冻成功" : "冻结成功");
        }
        return Result.error("修改异常!请联系管理员");
    }
}
