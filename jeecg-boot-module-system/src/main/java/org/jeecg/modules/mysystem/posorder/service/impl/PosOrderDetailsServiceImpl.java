package org.jeecg.modules.mysystem.posorder.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mysystem.posorder.entity.PosOrderDetails;
import org.jeecg.modules.mysystem.posorder.mapper.PosOrderDetailsMapper;
import org.jeecg.modules.mysystem.posorder.service.IPosOrderDetailsService;
import org.springframework.stereotype.Service;

/**
 * @Description: 订单主表
 * @Author: jeecg-boot
 * @Date: 2021-04-28
 * @Version: V1.0
 */
@Service
public class PosOrderDetailsServiceImpl extends ServiceImpl<PosOrderDetailsMapper, PosOrderDetails> implements IPosOrderDetailsService {

}
