package org.jeecg.modules.mysystem.posvipuser.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.config.utils.SmsSend;
import org.jeecg.modules.mysystem.posorder.entity.PosOrder;
import org.jeecg.modules.mysystem.posorder.entity.PosOrderDetails;
import org.jeecg.modules.mysystem.posorder.entity.VipUserBuyGoodsInfo;
import org.jeecg.modules.mysystem.posorder.service.IPosOrderDetailsService;
import org.jeecg.modules.mysystem.posorder.service.IPosOrderService;
import org.jeecg.modules.mysystem.posvipbirinfo.entity.PosVipBirInfo;
import org.jeecg.modules.mysystem.posvipbirinfo.service.IPosVipBirInfoService;
import org.jeecg.modules.mysystem.posvipintegral.entity.PosVipIntegral;
import org.jeecg.modules.mysystem.posvipintegral.service.IPosVipIntegralService;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUser;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUserVO;
import org.jeecg.modules.mysystem.posvipuser.service.IPosVipUserService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 会员信息
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
@Api(tags="会员信息")
@RestController
@RequestMapping("/posvipuser/posVipUser")
@Slf4j
public class PosVipUserController extends JeecgController<PosVipUser, IPosVipUserService> {
	@Autowired
	private IPosVipUserService posVipUserService;

	 @Autowired
	 private IPosVipIntegralService posVipIntegralService;

	 @Autowired
	 private SmsSend smsSend;

	 @Autowired
	 private IPosVipBirInfoService posVipBirInfoService;

	 @Autowired
	 private IPosOrderService posOrderService;

	 @Autowired
	 private IPosOrderDetailsService posOrderDetailsService;
	
	/**
	 * 分页列表查询
	 *
	 * @param posVipUserVO
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "会员信息-分页列表查询")
	@ApiOperation(value="会员信息-分页列表查询", notes="会员信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PosVipUserVO posVipUserVO,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   @RequestParam(value = "vipPhone",required = false) String vipPhone,
								   @RequestParam(value = "vipNumber",required = false) String vipNumber,
								   @RequestParam(value = "vipName",required = false) String vipName,
								   @RequestParam(value = "vipTag",required = false) String vipTag,
								   HttpServletRequest req) {
		QueryWrapper<PosVipUserVO> queryWrapper = QueryGenerator.initQueryWrapper(posVipUserVO, req.getParameterMap());
		Page<PosVipUserVO> page = new Page<PosVipUserVO>(pageNo, pageSize);
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		Map<String, Object> map = new HashMap<>();
		map.put("vipPhone",vipPhone);
		map.put("vipNumber",vipNumber);
		map.put("vipName",vipName);
		map.put("vipTag",vipTag);
		map.put("sysOrgCode",sysUser.getOrgCode());

		IPage<PosVipUserVO> pageList = posVipUserService.pages(page, map);
		return Result.ok(pageList);
	}

	/**
	 * @author: Mr.YSX
	 * @description: 会员生日筛选
	 * @date: 2021/4/27 14:37
	 * @return  /posvipuser/posVipUser/vipBir
	 */
	@AutoLog(value = "会员信息-分页列表查询")
	@ApiOperation(value="会员信息-分页列表查询", notes="会员信息-分页列表查询")
	@GetMapping(value = "/vipBir")
	public Result<?> vipBir(PosVipUser posVipUser,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PosVipUser> queryWrapper = QueryGenerator.initQueryWrapper(posVipUser, req.getParameterMap());
		Page<PosVipUser> page = new Page<PosVipUser>(pageNo, pageSize);
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		queryWrapper.eq("sys_org_code",sysUser.getOrgCode());
		queryWrapper.eq("vip_birthday",DateFormatUtils.format(new Date(), "yyyy-MM-dd"));
		IPage<PosVipUser> pageList = posVipUserService.page(page, queryWrapper);
		if (ObjectUtils.isEmpty(pageList)) {
			return Result.error("暂时还没有信息!");
		}
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param posVipUser
	 * @return
	 */
	@AutoLog(value = "会员信息-添加")
	@ApiOperation(value="会员信息-添加", notes="会员信息-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PosVipUser posVipUser) {
        QueryWrapper<PosVipUser> posVipUserQueryWrapper = new QueryWrapper<>();
        posVipUserQueryWrapper.eq("vip_number",posVipUser.getVipNumber());
        posVipUserQueryWrapper.eq("delete_flag",0);
        List<PosVipUser> list = this.posVipUserService.list(posVipUserQueryWrapper);
        if (!CollectionUtils.isEmpty(list)) {
            return Result.error("该会员卡号重复！");
        }

        QueryWrapper<PosVipUser> posVipUserQueryWrapperPhone = new QueryWrapper<>();
        posVipUserQueryWrapperPhone.eq("vip_phone",posVipUser.getVipPhone());
        posVipUserQueryWrapperPhone.eq("delete_flag",0);
        List<PosVipUser> listPhone = this.posVipUserService.list(posVipUserQueryWrapperPhone);
        if (!CollectionUtils.isEmpty(listPhone)) {
            return Result.error("该会员手机号重复！");
        }


        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		posVipUser.setSysOrgCode(sysUser.getOrgCode());
		posVipUser.setCreateBy(sysUser.getUsername());
		posVipUserService.save(posVipUser);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param posVipUser
	 * @return
	 */
	@AutoLog(value = "会员信息-编辑")
	@ApiOperation(value="会员信息-编辑", notes="会员信息-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PosVipUser posVipUser) {
        QueryWrapper<PosVipUser> posVipUserQueryWrapper = new QueryWrapper<>();
        posVipUserQueryWrapper.eq("vip_number",posVipUser.getVipNumber());
        posVipUserQueryWrapper.eq("delete_flag",0);
        List<PosVipUser> list = this.posVipUserService.list(posVipUserQueryWrapper);
        if (!CollectionUtils.isEmpty(list)) {
            return Result.error("该会员卡号重复！");
        }

        QueryWrapper<PosVipUser> posVipUserQueryWrapperPhone = new QueryWrapper<>();
        posVipUserQueryWrapperPhone.eq("vip_phone",posVipUser.getVipPhone());
        posVipUserQueryWrapperPhone.eq("delete_flag",0);
        List<PosVipUser> listPhone = this.posVipUserService.list(posVipUserQueryWrapperPhone);
        if (!CollectionUtils.isEmpty(listPhone)) {
            return Result.error("该会员手机号重复！");
        }


		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		posVipUser.setUpdateBy(sysUser.getOrgCode());
		posVipUser.setUpdateTime(new Date());
		posVipUserService.updateById(posVipUser);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员信息-通过id删除")
	@ApiOperation(value="会员信息-通过id删除", notes="会员信息-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (ObjectUtils.isEmpty(this.posVipUserService.getById(id))) {
			return Result.error("删除失败, 请刷新后再试!");
		}
		boolean b = this.posVipUserService.updateById(new PosVipUser().setId(id).setDeleteFlag(1).setUpdateTime(new Date()).setUpdateBy(sysUser.getOrgCode()));
		if (b){
			return Result.ok("删除成功!");
		}
		throw new JeecgBootException("删除失败, 请刷新后再试!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "会员信息-批量删除")
	@ApiOperation(value="会员信息-批量删除", notes="会员信息-批量删除")
//	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
//		this.posVipUserService.removeByIds(Arrays.asList(ids.split(",")));
		List<String> strings = Arrays.asList(ids.split(","));
		ArrayList<PosVipUser> posVipUsers = new ArrayList<>();
		for (String string : strings) {
			for (PosVipUser posVipUser : posVipUsers) {
				posVipUser.setDeleteFlag(1);
				posVipUser.setId(string);
			}
		}

		this.posVipUserService.updateBatchById(posVipUsers);
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员信息-通过id查询")
	@ApiOperation(value="会员信息-通过id查询", notes="会员信息-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PosVipUser posVipUser = posVipUserService.getById(id);
		if(posVipUser==null) {
			return Result.error("未找到对应数据");
		}
		if(posVipUser.getDeleteFlag().intValue() == 1) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(posVipUser);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param posVipUser
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PosVipUser posVipUser) {
        return super.exportXls(request, posVipUser, PosVipUser.class, "会员信息");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PosVipUser.class);
    }

    /**
     * @author: Mr.YSX
     * @description: 查询会员总余额
     * @date: 2021/4/22 17:38
     * @return  /posvipuser/posVipUser/balanceCount
     */
	@RequestMapping(value = "/balanceCount", method = RequestMethod.POST)
	public Result<?> balanceCount() {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		String orgCode = sysUser.getOrgCode();
		double count = this.posVipUserService.balanceCount(orgCode);
		return Result.ok(count);
	}

	/**
	 * @author: Mr.YSX
	 * @description: 编辑会员积分
	 * @date: 2021/4/24 12:19
	 * @param vipIntegral 积分  type '1'（加） '2'（减） remark（备注） id
	 * @return  /posvipuser/posVipUser/editUserIntegral
	 */
	@Transactional
	 @RequestMapping(value = "/editUserIntegral", method = RequestMethod.POST)
	 public Result<?> editUserIntegral(@RequestBody JSONObject jsonObject){
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 String orgCode = sysUser.getOrgCode();  //门店代码
		 if (StrUtil.isBlank(jsonObject.get("id").toString())) {
			 return Result.error("参数错误!");
		 }
		 String id = jsonObject.get("id").toString();
		 if (StrUtil.isBlank(jsonObject.get("integralCount").toString())) {
			 return Result.error("参数错误!");
		 }
		 BigDecimal vipIntegral = new BigDecimal(jsonObject.get("integralCount").toString());
		 if (StrUtil.isBlank(jsonObject.get("type").toString())) {
			 return Result.error("参数错误!");
		 }
		 String type = jsonObject.get("type").toString();
		 if (StrUtil.isBlank(jsonObject.get("remark").toString())) {
			 return Result.error("参数错误!");
		 }
		 String remark = jsonObject.get("remark").toString();
		 //查询会员积分信息
		 PosVipUser userInfo = this.posVipUserService.getById(id);
		if (ObjectUtils.isEmpty(userInfo)) {
			return Result.error("用户信息出错!");
		}
		 BigDecimal vipIntegralDB = userInfo.getVipIntegral();
		String types = "";
		 if (type.equals("2")) {
			 if (vipIntegralDB.compareTo(vipIntegral) == -1) {  // 用户积分小于要修改的积分
			 	return Result.error("用户积分小于要修改的积分");
			 }
			 //修改用户积分
			 BigDecimal subtract = vipIntegralDB.subtract(vipIntegral);
			 PosVipUser posVipUser2 = new PosVipUser();
			 posVipUser2.setId(id);
			 posVipUser2.setVipIntegral(subtract);
			 boolean b = this.posVipUserService.updateById(posVipUser2);
			 if (!b){
				 throw new JeecgBootException("修改失败,稍后尝试!");
			 }
			 types = "2";
		 }
		if (type.equals("1")) {
			//修改用户积分
			BigDecimal add = vipIntegralDB.add(vipIntegral);
			PosVipUser posVipUser1 = new PosVipUser();
			posVipUser1.setId(id);
			posVipUser1.setVipIntegral(add);
			boolean b = this.posVipUserService.updateById(posVipUser1);
			if (!b){
				throw new JeecgBootException("修改失败,稍后尝试!");
			}
			types = "1";
		}
		PosVipIntegral posVipIntegral = new PosVipIntegral();
		posVipIntegral.setSysOrgCode(orgCode);
		posVipIntegral.setType(types);
		posVipIntegral.setIntegralCount(vipIntegral);
		posVipIntegral.setRemark(remark);
		posVipIntegral.setCreateTime(new Date());

//		posVipIntegral.setVipPhoneI(id);
		posVipIntegral.setVipNameI(id);
//		posVipIntegral.setVipNumberI(id);
//		posVipIntegral.setVipIntegralI(id);
//		posVipIntegral.setVipSexI(id);
//		posVipIntegral.setVipBir(id);
		boolean save = this.posVipIntegralService.save(posVipIntegral);
		if (!save){
			throw new JeecgBootException("修改失败,稍后尝试!");
		}


		return Result.ok("修改成功!");
	 }

	 /**
	  * @author: Mr.YSX
	  * @description: 发送会员生日短信
	  * @date: 2021/4/27 15:39
	  * @param id(用户id) name（门店名称）
	  * @return /posvipuser/posVipUser/sendMs  post
	  */
	 @Transactional
	 @RequestMapping(value = "/sendMs", method = RequestMethod.POST)
	 public Result<?> sendCustomerBirthdaySms(@RequestBody JSONObject jsonObject) {
		 if (StrUtil.isBlank(jsonObject.get("id").toString())) {
			 return Result.error("参数错误!");
		 }
		 if (StrUtil.isBlank(jsonObject.get("name").toString())) {
			 return Result.error("参数错误!");
		 }
		 String id = jsonObject.get("id").toString();
		 PosVipUser byId = this.posVipUserService.getById(id);
		 if (ObjectUtils.isEmpty(byId)) {
			 return Result.error("会员不存在!");
		 }
		 String name = jsonObject.get("name").toString();
		 if (!smsSend.isMobileNO(byId.getVipPhone())) {
			 return Result.error("手机号格式不正确");
		 }
		 //查询是否已经发送过短信
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 QueryWrapper<PosVipBirInfo> queryWrapper = new QueryWrapper<>();
		 String dateString = this.getDateString();
		 queryWrapper.eq("vip_user_id",id);
		 queryWrapper.eq("vip_bir",dateString);
		 List<PosVipBirInfo> list = this.posVipBirInfoService.list(queryWrapper);

		 if (CollectionUtils.isEmpty(list)) {
			 boolean sms_195863297 = smsSend.sendBirthday(byId.getVipPhone(),name);
			 if(sms_195863297){
				//增加发送日志
				 PosVipBirInfo info = new PosVipBirInfo();
				 info.setVipUserId(id);
				 info.setVipBir(dateString);
				 info.setSysOrgCode(sysUser.getOrgCode());
				 this.posVipBirInfoService.save(info);
				 return Result.ok("发送成功");
			 }
			 return Result.error("短信发送失败");
		 } else {
			 return Result.error("已经发送过生日了....");
		 }

	 }


	 /**
	  * @author: Mr.YSX
	  * @description: 会员卡充值
	  * @date: 2021/4/27 16:54
	  * @param vipNumber（会员卡号） money（充值金额）
	  * @return
	  */
	 @AutoLog(value = "会员卡充值")
	 @ApiOperation(value="会员卡充值", notes="会员卡充值")
	 @PostMapping(value = "/vipTopUp")
	 public Result<?> vipTopUp(@RequestBody JSONObject jsonObject){
		 if (ObjectUtils.isEmpty(jsonObject.get("vipNumber")) && ObjectUtils.isEmpty(jsonObject.get("vipPhone"))) {
			 return Result.error("请传入会员卡号或手机号!");
		 }
		 if (ObjectUtils.isEmpty(jsonObject.get("money"))) {
			 return Result.error("请输入金额!");
		 }
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 HashMap<String, Object> map = new HashMap<>();
		 map.put("vipNumber",jsonObject.get("vipNumber"));
		 map.put("vipPhone",jsonObject.get("vipPhone"));
		 map.put("sysOrgCode",sysUser.getOrgCode());
		 String money = jsonObject.get("money").toString();         //充值金额
		 BigDecimal bigDecimal = new BigDecimal(money);
		 return this.posVipUserService.vipTopUp(map,bigDecimal);
	 }


       //获取当前日期的字符串 1
	 public String getDateString(){
		 String pattern = "yyyy-MM-dd";//获取的日期格式
		 SimpleDateFormat df = new SimpleDateFormat(pattern);
		 Date today = new Date();//获取当前日期
		 return df.format(today);//获取当前日期的字符串
	 }

	 /**
	  * @author: Mr.YSX
	  * @description: 前台通过会员卡号查询会员信息
	  * @date: 2021/4/28 16:58
	  * @param null
	  * @return  /posvipuser/posVipUser/queryVipInfoByVipNumber   post  参数 vipNumber vipPhone
	  */
	 @AutoLog(value = "前台通过会员卡号查询会员信息")
	 @ApiOperation(value="前台通过会员卡号查询会员信息", notes="前台通过会员卡号查询会员信息")
	 @PostMapping(value = "/queryVipInfoByVipNumber")
	 public Result<?> queryVipInfoByVipNumber(@RequestBody JSONObject jsonObject){
		 if (ObjectUtils.isEmpty(jsonObject.get("vipNumber")) && ObjectUtils.isEmpty(jsonObject.get("vipPhone"))) {
			 return Result.error("请传入会员卡号或手机号!");
		 }
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 HashMap<String, Object> map = new HashMap<>();
		 map.put("vipNumber",jsonObject.get("vipNumber"));
		 map.put("vipPhone",jsonObject.get("vipPhone"));
		 map.put("sysOrgCode",sysUser.getOrgCode());
		 PosVipUser posVipUser = this.posVipUserService.queryVipInfoByVipNumber(map);
		 if (ObjectUtils.isEmpty(posVipUser)) {
			 return Result.error("该会员不存在或已被删除!");
		 }
		 return Result.OK(posVipUser);
	 }

	 /**
	  * @author: Mr.YSX
	  * @description: 会员购买详情
	  * @date: 2021/5/5 17:38
	  * @param   vipCar 会员卡号
	  * @return   /posvipuser/posVipUser/queryUserBuyInfo
	  */
	 @AutoLog(value = "会员购买订单")
	 @ApiOperation(value="会员购买订单", notes="会员购买订单")
	 @GetMapping(value = "/queryUserBuyInfo")
	 public Result<?> queryUserBuyInfo(PosOrder posOrder,
									   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
//									   @RequestParam(name="vipCar",required = true) String vipCar,
									   HttpServletRequest req){
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 String orgCode = sysUser.getOrgCode();  // 门店代码
		 if (StringUtils.isEmpty(req.getParameter("vipCar"))){
			 return Result.error("参数错误!");
		 }
		 QueryWrapper<PosOrder> queryWrapper = QueryGenerator.initQueryWrapper(posOrder, req.getParameterMap());
		 Page<PosOrder> page = new Page<PosOrder>(pageNo, pageSize);
		 queryWrapper.eq("sys_org_code",orgCode);
		 queryWrapper.eq("vip_card",req.getParameter("vipCar"));
		 queryWrapper.eq("status",2);
		 queryWrapper.orderByDesc("create_time");
		 IPage<PosOrder> pageList = posOrderService.page(page, queryWrapper);
		 return Result.ok(pageList);
	 }


	 /**
	  * @author: Mr.YSX
	  * @description: 会员购买订单详情
	  * @date: 2021/5/5 18:57
	  * @param id 订单id
	  * @return
	  */
	 @AutoLog(value = "会员购买订单")
	 @ApiOperation(value="会员购买订单", notes="会员购买订单")
	 @GetMapping(value = "/queryUserBuyInfoDetails")
	 public Result<?> queryUserBuyInfoDetails(PosOrderDetails posOrderDetails,
											  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
											  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
											  @RequestParam(name="id",required = true) String id,
											  HttpServletRequest req){
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 String orgCode = sysUser.getOrgCode();  // 门店代码
		 if (StringUtils.isEmpty(id)){
			 return Result.error("参数错误!");
		 }

		 QueryWrapper<PosOrderDetails> queryWrapper = new QueryWrapper<PosOrderDetails>();
		 Page<PosOrderDetails> page = new Page<PosOrderDetails>(pageNo, pageSize);
		 queryWrapper.eq("sys_org_code",orgCode);
		 queryWrapper.eq("order_id",id);
		 queryWrapper.orderByDesc("create_time");
		 IPage<PosOrderDetails> pageList = this.posOrderDetailsService.page(page, queryWrapper);
		 return Result.ok(pageList);
	 }

	 /**
	  * @author: Mr.YSX
	  * @description: 会员购买商品详情
	  * @date: 2021/5/6 14:45
	  * @param vipCard  会员卡号
	  * @return
	  */
	 @AutoLog(value = "会员购买商品详情")
	 @ApiOperation(value="会员购买商品详情", notes="会员购买商品详情")
	 @GetMapping(value = "/queryUserBuyGoodsInfo")
	 public Result<?> queryUserBuyGoodsInfo(VipUserBuyGoodsInfo vipUserBuyGoodsInfo,
											@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
											@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
											@RequestParam(name="vipCard",required = true) String vipCard,
											HttpServletRequest req){
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 Page<VipUserBuyGoodsInfo> page = new Page<VipUserBuyGoodsInfo>(pageNo, pageSize);
		 if (StringUtils.isEmpty(vipCard)){
			 return Result.error("参数错误!");
		 }
		 Map<String, Object> map = new HashMap<>();
		 map.put("vipCard",vipCard);
		 map.put("sysOrgCode",sysUser.getOrgCode());

		 IPage<VipUserBuyGoodsInfo> pageList = this.posOrderService.pages(page, map);
		 return Result.ok(pageList);
	 }

}
