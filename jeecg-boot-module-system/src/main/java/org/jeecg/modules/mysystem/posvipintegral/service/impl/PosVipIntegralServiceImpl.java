package org.jeecg.modules.mysystem.posvipintegral.service.impl;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mysystem.posvipintegral.entity.PosVipIntegral;
import org.jeecg.modules.mysystem.posvipintegral.mapper.PosVipIntegralMapper;
import org.jeecg.modules.mysystem.posvipintegral.service.IPosVipIntegralService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 会员积分详情
 * @Author: jeecg-boot
 * @Date:   2021-04-24
 * @Version: V1.0
 */
@Service
public class PosVipIntegralServiceImpl extends ServiceImpl<PosVipIntegralMapper, PosVipIntegral> implements IPosVipIntegralService {

    /**
     * @author: Mr.YSX
     * @description: 结账增加用户积分消费信息
     * @date: 2021/5/5 16:42
     * @param integralCount 变化数量
     * @param type          变化方式   字符串 1 加     2 减     3 无
     * @param userId        会员id
     * @return
     */
    @Override
    public boolean saveIntegralInfo(BigDecimal integralCount, String type, String userId) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String username = sysUser.getUsername();
        int result = this.baseMapper.insert(new PosVipIntegral()
                .setCreateBy(username)
                .setVipNameI(userId)
                .setType(type)
                .setIntegralCount(integralCount)
                .setCreateTime(new Date())
                .setRemark("pos消费操作积分")
                .setSysOrgCode(sysUser.getOrgCode()));
        if (result == 1) {
            return true;
        }
        return false;
    }

    /**
     * @author: Mr.YSX
     * @description: 结账增加用户积分消费信息
     * @date: 2021/5/7 16:55
     * @param integralCount 变化数量
     * @param type          变化方式   字符串 1 加     2 减     3 无
     * @param userId        会员id
     * @param remark        备注
     * @return
     */
    @Override
    public boolean saveIntegralInfo(BigDecimal integralCount, String type, String userId, String remark) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String username = sysUser.getUsername();
        int result = this.baseMapper.insert(new PosVipIntegral()
                .setCreateBy(username)
                .setVipNameI(userId)
                .setType(type)
                .setIntegralCount(integralCount)
                .setCreateTime(new Date())
                .setRemark(remark)
                .setSysOrgCode(sysUser.getOrgCode()));
        if (result == 1) {
            return true;
        }
        return false;
    }
}
