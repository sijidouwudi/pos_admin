package org.jeecg.modules.mysystem.posvipintegral.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mysystem.posvipintegral.entity.PosVipIntegral;
import org.jeecg.modules.mysystem.posvipintegral.service.IPosVipIntegralService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 会员积分详情
 * @Author: jeecg-boot
 * @Date:   2021-04-24
 * @Version: V1.0
 */
@Api(tags="会员积分详情")
@RestController
@RequestMapping("/posvipintegral/posVipIntegral")
@Slf4j
public class PosVipIntegralController extends JeecgController<PosVipIntegral, IPosVipIntegralService> {
	@Autowired
	private IPosVipIntegralService posVipIntegralService;
	
	/**
	 * 分页列表查询
	 *
	 * @param posVipIntegral
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "会员积分详情-分页列表查询")
	@ApiOperation(value="会员积分详情-分页列表查询", notes="会员积分详情-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PosVipIntegral posVipIntegral,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   @RequestParam(name="vipId", defaultValue="10") String vipId,
								   HttpServletRequest req) {
		QueryWrapper<PosVipIntegral> queryWrapper = QueryGenerator.initQueryWrapper(posVipIntegral, req.getParameterMap());
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		queryWrapper.eq("sys_org_code",sysUser.getOrgCode());
		queryWrapper.eq("vip_name_i",vipId);
		Page<PosVipIntegral> page = new Page<PosVipIntegral>(pageNo, pageSize);
		IPage<PosVipIntegral> pageList = posVipIntegralService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param posVipIntegral
	 * @return
	 */
	@AutoLog(value = "会员积分详情-添加")
	@ApiOperation(value="会员积分详情-添加", notes="会员积分详情-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PosVipIntegral posVipIntegral) {
		posVipIntegralService.save(posVipIntegral);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param posVipIntegral
	 * @return
	 */
	@AutoLog(value = "会员积分详情-编辑")
	@ApiOperation(value="会员积分详情-编辑", notes="会员积分详情-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PosVipIntegral posVipIntegral) {
		posVipIntegralService.updateById(posVipIntegral);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员积分详情-通过id删除")
	@ApiOperation(value="会员积分详情-通过id删除", notes="会员积分详情-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		posVipIntegralService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "会员积分详情-批量删除")
	@ApiOperation(value="会员积分详情-批量删除", notes="会员积分详情-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.posVipIntegralService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员积分详情-通过id查询")
	@ApiOperation(value="会员积分详情-通过id查询", notes="会员积分详情-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PosVipIntegral posVipIntegral = posVipIntegralService.getById(id);
		if(posVipIntegral==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(posVipIntegral);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param posVipIntegral
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PosVipIntegral posVipIntegral) {
        return super.exportXls(request, posVipIntegral, PosVipIntegral.class, "会员积分详情");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PosVipIntegral.class);
    }

}
