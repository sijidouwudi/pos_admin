package org.jeecg.modules.mysystem.posvipquhuo.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mysystem.posvipquhuo.entity.PosVipQuhuo;
import org.jeecg.modules.mysystem.posvipquhuo.service.IPosVipQuhuoService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 会员取货
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
@Api(tags="会员取货")
@RestController
@RequestMapping("/posvipquhuo/posVipQuhuo")
@Slf4j
public class PosVipQuhuoController extends JeecgController<PosVipQuhuo, IPosVipQuhuoService> {
	@Autowired
	private IPosVipQuhuoService posVipQuhuoService;
	
	/**
	 * 分页列表查询
	 *
	 * @param posVipQuhuo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "会员取货-分页列表查询")
	@ApiOperation(value="会员取货-分页列表查询", notes="会员取货-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PosVipQuhuo posVipQuhuo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PosVipQuhuo> queryWrapper = QueryGenerator.initQueryWrapper(posVipQuhuo, req.getParameterMap());
		queryWrapper.eq("id",posVipQuhuo.getId());
		queryWrapper.eq("delete_flag",0);
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		queryWrapper.eq("sys_org_code",sysUser.getOrgCode());
		Page<PosVipQuhuo> page = new Page<PosVipQuhuo>(pageNo, pageSize);
		IPage<PosVipQuhuo> pageList = posVipQuhuoService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param posVipQuhuo
	 * @return
	 */
	@AutoLog(value = "会员取货-添加")
	@ApiOperation(value="会员取货-添加", notes="会员取货-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PosVipQuhuo posVipQuhuo) {
		posVipQuhuoService.save(posVipQuhuo);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param posVipQuhuo
	 * @return
	 */
	@AutoLog(value = "会员取货-编辑")
	@ApiOperation(value="会员取货-编辑", notes="会员取货-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PosVipQuhuo posVipQuhuo) {
		posVipQuhuoService.updateById(posVipQuhuo);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员取货-通过id删除")
	@ApiOperation(value="会员取货-通过id删除", notes="会员取货-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		posVipQuhuoService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "会员取货-批量删除")
	@ApiOperation(value="会员取货-批量删除", notes="会员取货-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.posVipQuhuoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员取货-通过id查询")
	@ApiOperation(value="会员取货-通过id查询", notes="会员取货-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PosVipQuhuo posVipQuhuo = posVipQuhuoService.getById(id);
		if(posVipQuhuo==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(posVipQuhuo);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param posVipQuhuo
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PosVipQuhuo posVipQuhuo) {
        return super.exportXls(request, posVipQuhuo, PosVipQuhuo.class, "会员取货");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PosVipQuhuo.class);
    }

}
