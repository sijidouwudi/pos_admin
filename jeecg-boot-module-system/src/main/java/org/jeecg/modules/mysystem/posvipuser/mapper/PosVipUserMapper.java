package org.jeecg.modules.mysystem.posvipuser.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUser;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUserVO;
import org.springframework.stereotype.Service;

import javax.websocket.server.PathParam;

/**
 * @Description: 会员信息
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
public interface PosVipUserMapper extends BaseMapper<PosVipUser> {

    /**
     * @author: Mr.YSX
     * @description: 查询会员总余额
     * @date: 2021/4/22 17:50
     * @return
     */
    @Select("SELECT SUM(vip_blance) FROM pos_vip_user WHERE sys_org_code = #{orgCode} and delete_flag = 0")
    double balanceCount(@PathParam("orgCode") String orgCode);

    /**
     * @author: Mr.YSX
     * @description: 会员信息-分页列表查询
     * @date: 2021/4/24 10:47
     * @return
     */
    IPage<PosVipUserVO> selectPages(Page<PosVipUserVO> page, @Param("params") Map<String, Object> map);


    PosVipUser queryVipInfoByVipNumber(@Param("params") HashMap<String, Object> map);
}
