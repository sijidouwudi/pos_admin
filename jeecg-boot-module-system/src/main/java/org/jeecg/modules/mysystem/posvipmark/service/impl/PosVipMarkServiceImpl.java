package org.jeecg.modules.mysystem.posvipmark.service.impl;

import org.jeecg.modules.mysystem.posvipmark.entity.PosVipMark;
import org.jeecg.modules.mysystem.posvipmark.mapper.PosVipMarkMapper;
import org.jeecg.modules.mysystem.posvipmark.service.IPosVipMarkService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 会员标签
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
@Service
public class PosVipMarkServiceImpl extends ServiceImpl<PosVipMarkMapper, PosVipMark> implements IPosVipMarkService {

}
