package org.jeecg.modules.mysystem.posinventorygoods.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 盘点商品
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
@Data
@TableName("pos_inventory_goods")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="pos_inventory_goods对象", description="盘点商品")
public class PosInventoryGoods implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "主键")
    private Long id;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**商品条码*/
	@Excel(name = "商品条码", width = 15)
    @ApiModelProperty(value = "商品条码")
    private String qrcode;
	/**商品名*/
	@Excel(name = "商品名", width = 15, dictTable = "pos_goods", dicText = "name", dicCode = "id")
	@Dict(dictTable = "pos_goods", dicText = "name", dicCode = "id")
    @ApiModelProperty(value = "商品名")
    private String name;
	/**商品单价*/
	@Excel(name = "商品单价", width = 15)
    @ApiModelProperty(value = "商品单价")
    private BigDecimal price;
	/**实际库存*/
	@Excel(name = "实际库存", width = 15)
    @ApiModelProperty(value = "实际库存")
    private BigDecimal count;
	/**盘点库存*/
	@Excel(name = "盘点库存", width = 15)
    @ApiModelProperty(value = "盘点库存")
    private BigDecimal inventoryCount;
	/**损耗库存*/
	@Excel(name = "损耗库存", width = 15)
    @ApiModelProperty(value = "损耗库存")
    private BigDecimal sunhao;
	/**盘点总金额*/
	@Excel(name = "盘点总金额", width = 15)
    @ApiModelProperty(value = "盘点总金额")
    private BigDecimal pandianPrice;
	/**损耗总金额*/
	@Excel(name = "损耗总金额", width = 15)
    @ApiModelProperty(value = "损耗总金额")
    private String sunhaoPrice;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**盘点单*/
	@Excel(name = "盘点单", width = 15)
    @ApiModelProperty(value = "盘点单")
    private String inventoryId;
}
