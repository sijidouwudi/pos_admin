package org.jeecg.modules.mysystem.posinventorygoods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posinventorygoods.entity.PosInventoryGoods;

/**
 * @Description: 盘点商品
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
public interface IPosInventoryGoodsService extends IService<PosInventoryGoods> {

}
