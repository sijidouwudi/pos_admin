package org.jeecg.modules.mysystem.possupplier.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.possupplier.entity.PosSupplier;

/**
 * @Description: 供应商
 * @Author: jeecg-boot
 * @Date:   2021-04-20
 * @Version: V1.0
 */
public interface PosSupplierMapper extends BaseMapper<PosSupplier> {

}
