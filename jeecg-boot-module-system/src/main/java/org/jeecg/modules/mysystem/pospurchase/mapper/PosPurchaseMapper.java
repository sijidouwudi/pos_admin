package org.jeecg.modules.mysystem.pospurchase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mysystem.pospurchase.entity.PosPurchase;

/**
 * @Description: 商品采购
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
public interface PosPurchaseMapper extends BaseMapper<PosPurchase> {

    IPage<PosPurchase> pages(Page<PosPurchase> page, @Param("params") PosPurchase queryWrapper);
}
