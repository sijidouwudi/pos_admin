package org.jeecg.modules.mysystem.posvipbalanceinfo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posvipbalanceinfo.entity.PosVipBalanceInfo;

import java.math.BigDecimal;

/**
 * @Description: 会员卡消费记录
 * @Author: jeecg-boot
 * @Date:   2021-04-28
 * @Version: V1.0
 */
public interface IPosVipBalanceInfoService extends IService<PosVipBalanceInfo> {


    /**
     * @author: Mr.YSX
     * @description: 前台使用会员卡余额支付消费记录
     * @date: 2021/5/7 16:55
     * @param infoClass      数字 1
     * @param before         变化前
     * @param after          变化后
     * @param count          变化值
     * @param type           变化方式   字符串 1 加     2 减     3 无
     * @param userId         会员id
     * @return
     */
    boolean saveBalancelInfo(int infoClass, BigDecimal before, BigDecimal after, BigDecimal count, String type, String userId);

}
