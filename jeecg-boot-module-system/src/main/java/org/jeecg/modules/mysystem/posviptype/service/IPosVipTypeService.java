package org.jeecg.modules.mysystem.posviptype.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posviptype.entity.PosVipType;

/**
 * @Description: 会员等级
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
public interface IPosVipTypeService extends IService<PosVipType> {

}
