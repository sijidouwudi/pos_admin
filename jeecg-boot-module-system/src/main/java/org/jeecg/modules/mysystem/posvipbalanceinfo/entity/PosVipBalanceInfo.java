package org.jeecg.modules.mysystem.posvipbalanceinfo.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 会员卡消费记录
 * @Author: jeecg-boot
 * @Date:   2021-04-28
 * @Version: V1.0
 */
@Data
@TableName("pos_vip_balance_info")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="pos_vip_balance_info对象", description="会员卡消费记录")
public class PosVipBalanceInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**会员id*/
	@Excel(name = "会员id", width = 15)
    @ApiModelProperty(value = "会员id")
    private String vipUserIdB;
	/**类型*/
	@Excel(name = "类型", width = 15, dicCode = "blance_leix")
	@Dict(dicCode = "blance_leix")
    @ApiModelProperty(value = "类型")
    private Integer type;
	/**余额变化前*/
	@Excel(name = "余额变化前", width = 15)
    @ApiModelProperty(value = "余额变化前")
    private BigDecimal moneyBefore;
	/**余额变化后*/
	@Excel(name = "余额变化后", width = 15)
    @ApiModelProperty(value = "余额变化后")
    private BigDecimal moneyAfter;
	/**余额变化值*/
	@Excel(name = "余额变化值", width = 15)
    @ApiModelProperty(value = "余额变化值")
    private BigDecimal money;
	/**操作类型*/
	@Excel(name = "操作类型", width = 15, dicCode = "way")
	@Dict(dicCode = "way")
    @ApiModelProperty(value = "操作类型")
    private Integer infoClass;
}
