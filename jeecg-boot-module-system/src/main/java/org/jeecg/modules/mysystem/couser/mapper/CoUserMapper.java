package org.jeecg.modules.mysystem.couser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mysystem.couser.entity.CoUser;


/**
 * @Description: 通用登录代码
 * @Author: jeecg-boot
 * @Date:   2020-09-02
 * @Version: V1.0
 */
public interface CoUserMapper extends BaseMapper<CoUser> {

    @Select("select * from co_user where number =#{number}")
    CoUser getUserByNumber(@Param("number") String accountNumber);
}
