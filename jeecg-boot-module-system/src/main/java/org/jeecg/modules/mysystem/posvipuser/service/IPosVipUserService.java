package org.jeecg.modules.mysystem.posvipuser.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUser;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUserVO;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 会员信息
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
public interface IPosVipUserService extends IService<PosVipUser> {

    /**
     * @author: Mr.YSX
     * @description: 查询会员总余额
     * @date: 2021/4/22 17:49
     * @return
     */
    double balanceCount(String orgCode);


    /**
     * @author: Mr.YSX
     * @description: 会员信息-分页列表查询
     * @date: 2021/4/24 10:46
     * @return
     */
    IPage<PosVipUserVO> pages(Page<PosVipUserVO> page, Map<String, Object> map);

    /**
     * @author: Mr.YSX
     * @description: 会员卡充值
     * @date: 2021/4/27 19:00
     * @param vipNumber  money
     * @return
     */
    Result<?> vipTopUp(HashMap<String, Object> map, BigDecimal money);

    PosVipUser queryVipInfoByVipNumber(HashMap<String, Object> map);
}
