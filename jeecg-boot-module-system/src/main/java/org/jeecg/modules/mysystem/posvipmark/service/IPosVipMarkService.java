package org.jeecg.modules.mysystem.posvipmark.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posvipmark.entity.PosVipMark;

/**
 * @Description: 会员标签
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
public interface IPosVipMarkService extends IService<PosVipMark> {

}
