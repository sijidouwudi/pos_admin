package org.jeecg.modules.mysystem.posviptopup.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posviptopup.entity.PosVipTopUp;

import javax.websocket.server.PathParam;

/**
 * @Description: 会员充值
 * @Author: jeecg-boot
 * @Date:   2021-04-26
 * @Version: V1.0
 */
public interface PosVipTopUpMapper extends BaseMapper<PosVipTopUp> {

    /**
     * @author: Mr.YSX
     * @description: 会员充值-分页列表查询
     * @date: 2021/4/26 16:22
     * @param page map
     * @return
     */
    IPage<PosVipTopUp> selectPages(Page<PosVipTopUp> page,@Param("params") Map<String, Object> map);
}
