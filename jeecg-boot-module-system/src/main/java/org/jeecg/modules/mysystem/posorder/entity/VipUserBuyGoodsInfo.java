package org.jeecg.modules.mysystem.posorder.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: pos_order_details
 * @Author: jeecg-boot
 * @Date:   2021-04-29
 * @Version: V1.0
 */
@Data
public class VipUserBuyGoodsInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**订单id*/
    private String orderId;
	/**购买日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "购买日期")
    private Date buyTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**商品名*/
	@Excel(name = "商品名", width = 15)
    @ApiModelProperty(value = "商品名")
    private String goodsName;
    /**类型名*/
    @Excel(name = "类型名", width = 15)
    @ApiModelProperty(value = "类型名")
    private String typeName;
    /**规格名*/
    @Excel(name = "规格名", width = 15)
    @ApiModelProperty(value = "规格名")
    private String goodsUnit;
    /**条码*/
    @Excel(name = "条码", width = 15)
    @ApiModelProperty(value = "条码")
    private String qrcode;

}
