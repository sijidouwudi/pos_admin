package org.jeecg.modules.mysystem.posvipintegral.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posvipintegral.entity.PosVipIntegral;

/**
 * @Description: 会员积分详情
 * @Author: jeecg-boot
 * @Date:   2021-04-24
 * @Version: V1.0
 */
public interface PosVipIntegralMapper extends BaseMapper<PosVipIntegral> {

}
