package org.jeecg.modules.mysystem.posvipmark.controller;

import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mysystem.posvipmark.entity.PosVipMark;
import org.jeecg.modules.mysystem.posvipmark.service.IPosVipMarkService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 会员标签
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
@Api(tags="会员标签")
@RestController
@RequestMapping("/posvipmark/posVipMark")
@Slf4j
public class PosVipMarkController extends JeecgController<PosVipMark, IPosVipMarkService> {
	@Autowired
	private IPosVipMarkService posVipMarkService;
	
	/**
	 * 分页列表查询
	 *
	 * @param posVipMark
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "会员标签-分页列表查询")
	@ApiOperation(value="会员标签-分页列表查询", notes="会员标签-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PosVipMark posVipMark,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PosVipMark> queryWrapper = QueryGenerator.initQueryWrapper(posVipMark, req.getParameterMap());
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		queryWrapper.eq("sys_org_code",sysUser.getOrgCode());
		queryWrapper.eq("delete_flag",0);
		Page<PosVipMark> page = new Page<PosVipMark>(pageNo, pageSize);
		IPage<PosVipMark> pageList = posVipMarkService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param posVipMark
	 * @return
	 */
	@AutoLog(value = "会员标签-添加")
	@ApiOperation(value="会员标签-添加", notes="会员标签-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PosVipMark posVipMark) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		posVipMark.setDeleteFlag(0);
		posVipMark.setSysOrgCode(sysUser.getOrgCode());
		posVipMarkService.save(posVipMark);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param posVipMark
	 * @return
	 */
	@AutoLog(value = "会员标签-编辑")
	@ApiOperation(value="会员标签-编辑", notes="会员标签-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PosVipMark posVipMark) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		posVipMark.setUpdateBy(sysUser.getOrgCode());
		posVipMarkService.updateById(posVipMark);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员标签-通过id删除")
	@ApiOperation(value="会员标签-通过id删除", notes="会员标签-通过id删除")
	@DeleteMapping(value = "/delete")
	@Transactional
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (ObjectUtils.isEmpty(this.posVipMarkService.getById(id))) {
			return Result.error("删除失败, 请刷新后再试!");
		}
		boolean b = this.posVipMarkService.updateById(new PosVipMark().setId(id).setDeleteFlag(1).setUpdateTime(new Date()).setUpdateBy(sysUser.getOrgCode()));
		if (b){
			return Result.ok("删除成功!");
		}
		throw new JeecgBootException("删除失败, 请刷新后再试!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "会员标签-批量删除")
	@ApiOperation(value="会员标签-批量删除", notes="会员标签-批量删除")
//	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.posVipMarkService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员标签-通过id查询")
	@ApiOperation(value="会员标签-通过id查询", notes="会员标签-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PosVipMark posVipMark = posVipMarkService.getById(id);
		if(posVipMark==null) {
			return Result.error("未找到对应数据");
		}
		if(posVipMark.getDeleteFlag().intValue() == 1) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(posVipMark);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param posVipMark
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PosVipMark posVipMark) {
        return super.exportXls(request, posVipMark, PosVipMark.class, "会员标签");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PosVipMark.class);
    }

}
