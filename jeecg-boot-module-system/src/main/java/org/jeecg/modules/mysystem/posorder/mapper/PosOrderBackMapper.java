package org.jeecg.modules.mysystem.posorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posorder.entity.PosOrderBack;

/**
 * @Description: 退货订单
 * @Author: jeecg-boot
 * @Date:   2021-05-07
 * @Version: V1.0
 */
public interface PosOrderBackMapper extends BaseMapper<PosOrderBack> {

}
