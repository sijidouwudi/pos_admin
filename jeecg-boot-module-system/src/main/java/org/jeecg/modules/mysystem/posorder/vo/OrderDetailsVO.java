package org.jeecg.modules.mysystem.posorder.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author yuanxin
 * @Date 2021/4/29 16:44
 **/
@Data
public class OrderDetailsVO {

    private String goodsName;

    private String goodsId;

    private String count;

    private BigDecimal price;

    private Long version;

    private String isActivity;


}