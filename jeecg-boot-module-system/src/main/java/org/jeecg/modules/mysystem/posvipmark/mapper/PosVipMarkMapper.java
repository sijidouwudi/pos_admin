package org.jeecg.modules.mysystem.posvipmark.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posvipmark.entity.PosVipMark;

/**
 * @Description: 会员标签
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
public interface PosVipMarkMapper extends BaseMapper<PosVipMark> {

}
