package org.jeecg.modules.mysystem.posvipquhuo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posvipquhuo.entity.PosVipQuhuo;

/**
 * @Description: 会员取货
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
public interface IPosVipQuhuoService extends IService<PosVipQuhuo> {

}
