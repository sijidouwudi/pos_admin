package org.jeecg.modules.mysystem.posorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.lettuce.core.dynamic.annotation.Param;
import org.jeecg.modules.mysystem.posorder.entity.PosOrder;
import org.jeecg.modules.mysystem.posorder.entity.VipUserBuyGoodsInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 订单主表
 * @Author: jeecg-boot
 * @Date: 2021-04-28
 * @Version: V1.0
 */
public interface PosOrderMapper extends BaseMapper<PosOrder> {

    Page<Map> queryPendingOrder(Page page, String orgCode);

    /**
     * @param map
     * @return
     * @author: Mr.YSX
     * @description: 会员购买商品详情
     * @date: 2021/5/6 14:54
     */
    IPage<VipUserBuyGoodsInfo> selectPages(Page<VipUserBuyGoodsInfo> page, @Param("params") Map<String, Object> map);

    List<Map<String, Object>> endPendingOrder(String id);

    void deleteOrderAndDetails(String id);

    void updateOrder(String id);

    Page<Map> queryRetractOrder(Page page,HashMap<String, String> map);
}
