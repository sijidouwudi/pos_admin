package org.jeecg.modules.mysystem.posorder.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posorder.entity.PosOrderDetails;

/**
 * @Description: 订单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-28
 * @Version: V1.0
 */
public interface IPosOrderDetailsService extends IService<PosOrderDetails> {

}
