package org.jeecg.modules.mysystem.posvipbalanceinfo.service.impl;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mysystem.posvipbalanceinfo.entity.PosVipBalanceInfo;
import org.jeecg.modules.mysystem.posvipbalanceinfo.mapper.PosVipBalanceInfoMapper;
import org.jeecg.modules.mysystem.posvipbalanceinfo.service.IPosVipBalanceInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 会员卡消费记录
 * @Author: jeecg-boot
 * @Date:   2021-04-28
 * @Version: V1.0
 */
@Service
public class PosVipBalanceInfoServiceImpl extends ServiceImpl<PosVipBalanceInfoMapper, PosVipBalanceInfo> implements IPosVipBalanceInfoService {

    /**
     * @author: Mr.YSX
     * @description: 前台使用会员卡余额支付消费记录
     * @date: 2021/5/7 16:55
     * @param infoClass      数字 1
     * @param before         变化前
     * @param after          变化后
     * @param count          变化值
     * @param type           变化方式   字符串 1 加     2 减     3 无
     * @param userId         会员id
     * @return
     */
    @Override
    public boolean saveBalancelInfo(int infoClass, BigDecimal before, BigDecimal after, BigDecimal count, String type, String userId) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String username = sysUser.getUsername();
        int insert = this.baseMapper.insert(new PosVipBalanceInfo()
                .setCreateBy(username)
                .setInfoClass(infoClass)
                .setMoneyBefore(before)
                .setMoneyAfter(after)
                .setMoney(count)
                .setVipUserIdB(userId)
                .setType(2)
                .setSysOrgCode(sysUser.getOrgCode())
                .setCreateTime(new Date()));
        if (insert == 1) {
            return true;
        }
        return false;
    }
}
