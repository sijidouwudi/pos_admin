package org.jeecg.modules.mysystem.posvipbirinfo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mysystem.posvipbirinfo.entity.PosVipBirInfo;

/**
 * @Description: 会员生日发送日志
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
public interface IPosVipBirInfoService extends IService<PosVipBirInfo> {

}
