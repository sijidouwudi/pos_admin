package org.jeecg.modules.mysystem.posvipcunhuo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mysystem.posvipcunhuo.entity.PosVipCunhuo;

import javax.websocket.server.PathParam;

/**
 * @Description: 会员存货
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
public interface PosVipCunhuoMapper extends BaseMapper<PosVipCunhuo> {


    /**
     * @author: Mr.YSX
     * @description: 会员存货总金额
     * @date: 2021/4/23 14:14
     * @return
     */
    @Select("SELECT SUM(amount) FROM pos_vip_cunhuo WHERE sys_org_code = #{OrgCode} and delete_flag = 0")
    double amountCount(@PathParam("OrgCode") String OrgCode);
}
