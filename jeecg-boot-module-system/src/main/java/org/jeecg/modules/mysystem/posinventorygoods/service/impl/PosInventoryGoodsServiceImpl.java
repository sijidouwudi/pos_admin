package org.jeecg.modules.mysystem.posinventorygoods.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mysystem.posinventorygoods.entity.PosInventoryGoods;
import org.jeecg.modules.mysystem.posinventorygoods.mapper.PosInventoryGoodsMapper;
import org.jeecg.modules.mysystem.posinventorygoods.service.IPosInventoryGoodsService;
import org.springframework.stereotype.Service;

/**
 * @Description: 盘点商品
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
@Service
public class PosInventoryGoodsServiceImpl extends ServiceImpl<PosInventoryGoodsMapper, PosInventoryGoods> implements IPosInventoryGoodsService {

}
