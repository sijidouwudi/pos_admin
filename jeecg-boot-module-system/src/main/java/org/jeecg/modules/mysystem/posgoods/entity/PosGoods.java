package org.jeecg.modules.mysystem.posgoods.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 商品表
 * @Author: jeecg-boot
 * @Date: 2021-04-20
 * @Version: V1.0
 */
@Data
@TableName("pos_goods")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "pos_goods对象", description = "商品表")
public class PosGoods implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createBy;
    /**
     * 创建日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private String updateBy;
    /**
     * 更新日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
    /**
     * 所属部门
     */
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
    /**
     * 商品名
     */
    @Excel(name = "商品名", width = 15)
    @ApiModelProperty(value = "商品名")
    private String name;
    /**
     * 条码
     */
    @Excel(name = "条码", width = 15)
    @ApiModelProperty(value = "条码")
    private String qrcode;
    /**
     * 是否上架
     */
    @Excel(name = "是否上架", width = 15)
    @ApiModelProperty(value = "是否上架")
    private String status;
    /**
     * 计量方式
     */
    @Excel(name = "计量方式", width = 15, dicCode = "jl")
    @Dict(dicCode = "jl")
    @ApiModelProperty(value = "计量方式")
    private String isWeight;
    /**
     * 商品类别
     */
    @Excel(name = "商品类别", width = 15, dictTable = "pos_goods_type", dicText = "name", dicCode = "id")
    @Dict(dictTable = "pos_goods_type", dicText = "name", dicCode = "id")
    @ApiModelProperty(value = "商品类别")
    private String goodsType;
    /**
     * 进价
     */
    @Excel(name = "进价", width = 15)
    @ApiModelProperty(value = "进价")
    private BigDecimal jjPrice;
    /**
     * 零售价
     */
    @Excel(name = "零售价", width = 15)
    @ApiModelProperty(value = "零售价")
    private BigDecimal lsPrice;
    /**
     * 活动价
     */
    @Excel(name = "活动价", width = 15)
    @ApiModelProperty(value = "活动价")
    private BigDecimal hdPrice;
    /**
     * 会员价
     */
    @Excel(name = "会员价", width = 15)
    @ApiModelProperty(value = "会员价")
    private BigDecimal vipPrice;
    /**
     * 规格
     */
    @Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private String goodsUnit;
    /**
     * 获得积分
     */
    @Excel(name = "获得积分", width = 15)
    @ApiModelProperty(value = "获得积分")
    private BigDecimal points;
    /**
     * 库存
     */
    @Excel(name = "库存", width = 15)
    @ApiModelProperty(value = "库存")
    private BigDecimal count;
    /**
     * 保质期(月)
     */
    @Excel(name = "保质期(月)", width = 15)
    @ApiModelProperty(value = "保质期(月)")
    private BigDecimal saveDate;
    /**
     * 损耗
     */
    @Excel(name = "损耗", width = 15)
    @ApiModelProperty(value = "损耗")
    private BigDecimal sunhao;

    /**
     * 删除标识
     */
    @Excel(name = "1删除2未删除", width = 15)
    @ApiModelProperty(value = "1删除2未删除")
    private String delFlag;

    /**
     * 状态
     */
    @Excel(name = "版本状态(乐观锁)", width = 15)
    @ApiModelProperty(value = "版本状态(乐观锁)")
    private Long version;

    @TableField(exist = false)
    private BigDecimal weight;

    /**兑换所需积分*/
    @Excel(name = "兑换所需积分", width = 15)
    @ApiModelProperty(value = "兑换所需积分")
    private BigDecimal dhNeedPoints;

    @Excel(name = "是否是积分兑换", width = 15)
    @ApiModelProperty(value = "是否是积分兑换")
    @Dict(dicCode = "isPointsDhs")
    private Integer isPointsDh;   // 1设为积分兑换   2 不设为积分兑换 默认2
}
