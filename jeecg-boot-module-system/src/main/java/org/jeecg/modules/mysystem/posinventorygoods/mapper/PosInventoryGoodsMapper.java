package org.jeecg.modules.mysystem.posinventorygoods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posinventorygoods.entity.PosInventoryGoods;

/**
 * @Description: 盘点商品
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
public interface PosInventoryGoodsMapper extends BaseMapper<PosInventoryGoods> {

    void batchInsertGoods(String orgcode,String id);
}
