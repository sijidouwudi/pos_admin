package org.jeecg.modules.mysystem.posinventory.service.impl;


import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.modules.mysystem.posgoods.mapper.PosGoodsMapper;
import org.jeecg.modules.mysystem.posinventory.entity.PosInventory;
import org.jeecg.modules.mysystem.posinventory.mapper.PosInventoryMapper;
import org.jeecg.modules.mysystem.posinventory.service.IPosInventoryService;
import org.jeecg.modules.mysystem.posinventorygoods.entity.PosInventoryGoods;
import org.jeecg.modules.mysystem.posinventorygoods.mapper.PosInventoryGoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 库存盘点
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
@Service
public class PosInventoryServiceImpl extends ServiceImpl<PosInventoryMapper, PosInventory> implements IPosInventoryService {

    @Autowired
    private PosInventoryGoodsMapper posInventoryGoodsMapper;

    @Autowired
    private PosGoodsMapper posGoodsMapper;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void excuteService(PosInventory posInventory) throws JeecgBootException {
        if("2".equals(posInventory.getStatus())){
            //同步修改商品库存
            List<PosInventoryGoods> posInventoryGoods = posInventoryGoodsMapper.selectList(new QueryWrapper<PosInventoryGoods>(new PosInventoryGoods().setInventoryId(posInventory.getId())));
            if(CollUtil.isNotEmpty(posInventoryGoods)){
                posGoodsMapper.batchUpdstes(posInventoryGoods);
            }
        }
    }
}
