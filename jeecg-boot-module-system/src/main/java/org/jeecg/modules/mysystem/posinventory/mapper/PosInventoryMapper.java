package org.jeecg.modules.mysystem.posinventory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posinventory.entity.PosInventory;

/**
 * @Description: 库存盘点
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
public interface PosInventoryMapper extends BaseMapper<PosInventory> {

}
