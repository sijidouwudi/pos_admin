package org.jeecg.modules.mysystem.posvipquhuo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posvipquhuo.entity.PosVipQuhuo;

/**
 * @Description: 会员取货
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
public interface PosVipQuhuoMapper extends BaseMapper<PosVipQuhuo> {

}
