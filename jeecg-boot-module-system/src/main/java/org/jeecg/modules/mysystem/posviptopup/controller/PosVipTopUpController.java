package org.jeecg.modules.mysystem.posviptopup.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mysystem.posviptopup.entity.PosVipTopUp;
import org.jeecg.modules.mysystem.posviptopup.service.IPosVipTopUpService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 会员充值
 * @Author: jeecg-boot
 * @Date:   2021-04-26
 * @Version: V1.0
 */
@Api(tags="会员充值")
@RestController
@RequestMapping("/posviptopup/posVipTopUp")
@Slf4j
public class PosVipTopUpController extends JeecgController<PosVipTopUp, IPosVipTopUpService> {
	@Autowired
	private IPosVipTopUpService posVipTopUpService;
	
	/**
	 * 分页列表查询
	 *
	 * @param posVipTopUp
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "会员充值-分页列表查询")
	@ApiOperation(value="会员充值-分页列表查询", notes="会员充值-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PosVipTopUp posVipTopUp,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   @RequestParam(name="createTimeBegin", required = false) String createTimeBegin,
								   @RequestParam(name="createTimeEnd", required = false) String createTimeEnd,
								   @RequestParam(name="vipNameTop", required = false) String vipNameTop,
								   @RequestParam(name="vipNumberTop", required = false) String vipNumberTop,
								   @RequestParam(name="vipPhoneTop", required = false) String vipPhoneTop,
								   HttpServletRequest req) {
		QueryWrapper<PosVipTopUp> queryWrapper = QueryGenerator.initQueryWrapper(posVipTopUp, req.getParameterMap());
		Page<PosVipTopUp> page = new Page<PosVipTopUp>(pageNo, pageSize);
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		Map<String, Object> map = new HashMap<>();
		map.put("createTimeBegin",createTimeBegin);
		map.put("createTimeEnd",createTimeEnd);
		map.put("vipNameTop",vipNameTop);
		map.put("vipNumberTop",vipNumberTop);
		map.put("vipPhoneTop",vipPhoneTop);
		map.put("sysOrgCode",sysUser.getOrgCode());
//		System.out.println("接收参数 "+map);

		IPage<PosVipTopUp> pageList = posVipTopUpService.pages(page, map);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param posVipTopUp
	 * @return
	 */
	@AutoLog(value = "会员充值-添加")
	@ApiOperation(value="会员充值-添加", notes="会员充值-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PosVipTopUp posVipTopUp) {
		posVipTopUpService.save(posVipTopUp);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param posVipTopUp
	 * @return
	 */
	@AutoLog(value = "会员充值-编辑")
	@ApiOperation(value="会员充值-编辑", notes="会员充值-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PosVipTopUp posVipTopUp) {
		posVipTopUpService.updateById(posVipTopUp);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员充值-通过id删除")
	@ApiOperation(value="会员充值-通过id删除", notes="会员充值-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		posVipTopUpService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "会员充值-批量删除")
	@ApiOperation(value="会员充值-批量删除", notes="会员充值-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.posVipTopUpService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员充值-通过id查询")
	@ApiOperation(value="会员充值-通过id查询", notes="会员充值-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PosVipTopUp posVipTopUp = posVipTopUpService.getById(id);
		if(posVipTopUp==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(posVipTopUp);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param posVipTopUp
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PosVipTopUp posVipTopUp) {
        return super.exportXls(request, posVipTopUp, PosVipTopUp.class, "会员充值");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PosVipTopUp.class);
    }

}
