package org.jeecg.modules.mysystem.posgoods.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mysystem.posgoods.entity.PosGoods;
import org.jeecg.modules.mysystem.posgoods.service.IPosGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 商品表
 * @Author: jeecg-boot
 * @Date: 2021-04-20
 * @Version: V1.0
 */
@Api(tags = "商品表")
@RestController
@RequestMapping("/posgoods/posGoods")
@Slf4j
public class PosGoodsController extends JeecgController<PosGoods, IPosGoodsService> {
    @Autowired
    private IPosGoodsService posGoodsService;



    /**
     * 查询单个商品
     * @param id
     * @return org.jeecg.common.api.vo.Result<?>
     */
    @AutoLog(value = "单个商品查询")
    @ApiOperation(value = "单个商品查询", notes = "单个商品查询")
    @GetMapping(value = "/qrcode")
    public Result<?> qrcode(@RequestParam String id)throws JeecgBootException {
        return posGoodsService.executeService(id);
    }

    /**
     * 分页列表查询
     *
     * @param posGoods
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "商品表-分页列表查询")
    @ApiOperation(value = "商品表-分页列表查询", notes = "商品表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(PosGoods posGoods,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<PosGoods> queryWrapper = QueryGenerator.initQueryWrapper(posGoods, req.getParameterMap());
        Page<PosGoods> page = new Page<PosGoods>(pageNo, pageSize);
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        queryWrapper.eq("sys_org_code", sysUser.getOrgCode());
        queryWrapper.eq("del_flag", "1");
        IPage<PosGoods> pageList = posGoodsService.page(page, queryWrapper);
        return Result.ok(pageList);
    }


    /**
     * 分页列表查询
     *
     * @param posGoods
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "商品表-分页列表查询积分兑换商品")
    @ApiOperation(value = "商品表-分页列表查询积分兑换商品", notes = "商品表-分页列表查询积分兑换商品")
    @GetMapping(value = "/queryPageListByPointGoods")
    public Result<?> queryPageListByPointGoods(PosGoods posGoods,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<PosGoods> queryWrapper = QueryGenerator.initQueryWrapper(posGoods, req.getParameterMap());
        Page<PosGoods> page = new Page<PosGoods>(pageNo, pageSize);
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        queryWrapper.eq("sys_org_code", sysUser.getOrgCode());
        queryWrapper.eq("del_flag", "1");
        queryWrapper.eq("is_points_dh", 1);
        queryWrapper.eq("status", "Y");
        IPage<PosGoods> pageList = posGoodsService.page(page, queryWrapper);
        if (CollectionUtils.isEmpty(pageList.getRecords())) {
            Result.ok("暂时没有积分兑换的商品!");
        }
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param posGoods
     * @return
     */
    @AutoLog(value = "商品表-添加")
    @ApiOperation(value = "商品表-添加", notes = "商品表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody PosGoods posGoods) {
        posGoodsService.save(posGoods);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param posGoods
     * @return
     */
    @AutoLog(value = "商品表-编辑")
    @ApiOperation(value = "商品表-编辑", notes = "商品表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody PosGoods posGoods) {
        posGoodsService.updateById(posGoods);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "商品表-通过id删除")
    @ApiOperation(value = "商品表-通过id删除", notes = "商品表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        PosGoods posGoods = new PosGoods().setId(id).setDelFlag("2");
        posGoodsService.updateById(posGoods);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "商品表-批量删除")
    @ApiOperation(value = "商品表-批量删除", notes = "商品表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.posGoodsService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "商品表-通过id查询")
    @ApiOperation(value = "商品表-通过id查询", notes = "商品表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        PosGoods posGoods = posGoodsService.getById(id);
        if (posGoods == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(posGoods);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param posGoods
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PosGoods posGoods) {
        return super.exportXls(request, posGoods, PosGoods.class, "商品表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PosGoods.class);
    }


    /**
     * @author: Mr.YSX
     * @description: pos前台积分兑换商品
     * @date: 2021/5/7 16:08
     * @param jsonObject  vipCard  会员卡号
     * @param jsonObject  goodId   商品id
     * @param jsonObject  number   兑换数量
     * @return  /posgoods/posGoods/pointDhGoods
     */
    @AutoLog(value = "pos前台积分兑换商品")
    @ApiOperation(value = "pos前台积分兑换商品", notes = "pos前台积分兑换商品")
    @PostMapping(value = "/pointDhGoods")
    public Result<?> pointDhGoods(@RequestBody JSONObject jsonObject){
        if (StringUtils.isEmpty(jsonObject.get("vipCard"))
                || StringUtils.isEmpty(jsonObject.get("goodId"))
                || StringUtils.isEmpty(jsonObject.get("number"))) {
            return Result.error("缺少参数!");
        }
        String vipCard = jsonObject.get("vipCard").toString();
        String goodId = jsonObject.get("goodId").toString();
        String number = jsonObject.get("number").toString();
        return this.posGoodsService.pointDhGoods(vipCard,goodId,number);
    }


}
