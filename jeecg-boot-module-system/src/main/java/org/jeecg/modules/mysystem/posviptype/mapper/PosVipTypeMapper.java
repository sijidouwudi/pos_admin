package org.jeecg.modules.mysystem.posviptype.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posviptype.entity.PosVipType;

/**
 * @Description: 会员等级
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
public interface PosVipTypeMapper extends BaseMapper<PosVipType> {

}
