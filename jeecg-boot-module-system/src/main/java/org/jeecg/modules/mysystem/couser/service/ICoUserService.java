package org.jeecg.modules.mysystem.couser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mysystem.couser.entity.CoUser;

/**
 * @Description: 通用登录代码
 * @Author: jeecg-boot
 * @Date:   2020-09-02
 * @Version: V1.0
 */
public interface ICoUserService extends IService<CoUser> {

    Result<?> freezeTheAccount(CoUser map);
}
