package org.jeecg.modules.mysystem.posvipbirinfo.service.impl;

import org.jeecg.modules.mysystem.posvipbirinfo.entity.PosVipBirInfo;
import org.jeecg.modules.mysystem.posvipbirinfo.mapper.PosVipBirInfoMapper;
import org.jeecg.modules.mysystem.posvipbirinfo.service.IPosVipBirInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 会员生日发送日志
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
@Service
public class PosVipBirInfoServiceImpl extends ServiceImpl<PosVipBirInfoMapper, PosVipBirInfo> implements IPosVipBirInfoService {

}
