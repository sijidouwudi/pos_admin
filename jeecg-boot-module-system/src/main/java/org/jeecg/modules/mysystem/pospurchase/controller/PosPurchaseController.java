package org.jeecg.modules.mysystem.pospurchase.controller;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.mysystem.posgoods.service.IPosGoodsService;
import org.jeecg.modules.mysystem.posinventory.entity.PosInventory;
import org.jeecg.modules.mysystem.posinventory.mapper.PosInventoryMapper;
import org.jeecg.modules.mysystem.pospurchase.entity.PosPurchase;
import org.jeecg.modules.mysystem.pospurchase.service.IPosPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 商品采购
 * @Author: jeecg-boot
 * @Date: 2021-04-23
 * @Version: V1.0
 */
@Api(tags = "商品采购")
@RestController
@RequestMapping("/pospurchase/posPurchase")
@Slf4j
public class PosPurchaseController extends JeecgController<PosPurchase, IPosPurchaseService> {
    @Autowired
    private IPosPurchaseService posPurchaseService;
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private IPosGoodsService iPosGoodsService;

    @Autowired
    private PosInventoryMapper posInventoryMapper;

    /**
     * 分页列表查询
     *
     * @param posPurchase
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "商品采购-分页列表查询")
    @ApiOperation(value = "商品采购-分页列表查询", notes = "商品采购-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(PosPurchase posPurchase,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        Page<PosPurchase> page = new Page<PosPurchase>(pageNo, pageSize);
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        posPurchase.setSysOrgCode(sysUser.getOrgCode());
        IPage<PosPurchase> pageList = posPurchaseService.pages(page, posPurchase);
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param posPurchase
     * @return
     */
    @AutoLog(value = "商品采购-添加")
    @ApiOperation(value = "商品采购-添加", notes = "商品采购-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody PosPurchase posPurchase) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper queryWrapper = new QueryWrapper<PosInventory>(new PosInventory().setStatus("1").setSysOrgCode(sysUser.getOrgCode()));
        List list = posInventoryMapper.selectList(queryWrapper);
        if (CollUtil.isNotEmpty(list)) {
            return Result.error("后台存在盘点不能使用结算功能");
        }
        posPurchaseService.save(posPurchase);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param posPurchase
     * @return
     */
    @AutoLog(value = "商品采购-编辑")
    @ApiOperation(value = "商品采购-编辑", notes = "商品采购-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody PosPurchase posPurchase) {
        //防止表单重复提交
        long incr = redisUtil.incr(posPurchase.getId() + posPurchase.getStatus(), 1);
        if (incr != 1) {
            throw new JeecgBootException("不要重复点击请10秒钟后再试");
        }
        redisUtil.expire(posPurchase.getId() + posPurchase.getStatus(), 10);
        iPosGoodsService.goodsCountsSaveProcess(posPurchase);
        posPurchaseService.updateById(posPurchase);
        return Result.ok("编辑成功!");
    }

    /**
     * 编辑
     *
     * @param posPurchase
     * @return
     */
    @AutoLog(value = "商品采购-编辑")
    @ApiOperation(value = "商品采购-编辑", notes = "商品采购-编辑")
    @PutMapping(value = "/editpricestatus")
    public Result<?> editpricestatus(@RequestBody PosPurchase posPurchase) {
        posPurchaseService.updateById(posPurchase);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "商品采购-通过id删除")
    @ApiOperation(value = "商品采购-通过id删除", notes = "商品采购-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        posPurchaseService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "商品采购-批量删除")
    @ApiOperation(value = "商品采购-批量删除", notes = "商品采购-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.posPurchaseService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "商品采购-通过id查询")
    @ApiOperation(value = "商品采购-通过id查询", notes = "商品采购-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        PosPurchase posPurchase = posPurchaseService.getById(id);
        if (posPurchase == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(posPurchase);
    }


}
