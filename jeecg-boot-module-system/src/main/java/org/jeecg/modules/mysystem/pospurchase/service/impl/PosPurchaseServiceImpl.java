package org.jeecg.modules.mysystem.pospurchase.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mysystem.pospurchase.entity.PosPurchase;
import org.jeecg.modules.mysystem.pospurchase.mapper.PosPurchaseMapper;
import org.jeecg.modules.mysystem.pospurchase.service.IPosPurchaseService;
import org.springframework.stereotype.Service;

/**
 * @Description: 商品采购
 * @Author: jeecg-boot
 * @Date:   2021-04-23
 * @Version: V1.0
 */
@Service
public class PosPurchaseServiceImpl extends ServiceImpl<PosPurchaseMapper, PosPurchase> implements IPosPurchaseService {

    @Override
    public IPage<PosPurchase> pages(Page<PosPurchase> page, PosPurchase queryWrapper) {
        return baseMapper.pages(page,queryWrapper);
    }
}
