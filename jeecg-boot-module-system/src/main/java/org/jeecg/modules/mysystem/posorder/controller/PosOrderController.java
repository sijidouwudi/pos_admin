package org.jeecg.modules.mysystem.posorder.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mysystem.posorder.entity.PosOrder;
import org.jeecg.modules.mysystem.posorder.mapper.PosOrderMapper;
import org.jeecg.modules.mysystem.posorder.service.IPosOrderService;
import org.jeecg.modules.mysystem.posorder.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 订单主表
 * @Author: jeecg-boot
 * @Date: 2021-04-28
 * @Version: V1.0
 */
@Api(tags = "订单主表")
@RestController
@RequestMapping("/posorder/posOrder")
@Slf4j
public class PosOrderController extends JeecgController<PosOrder, IPosOrderService> {
    @Autowired
    private IPosOrderService posOrderService;

    @Autowired
    private PosOrderMapper posOrderMapper;


    /**
     * 添加
     *
     * @param posOrder
     * @return
     */
    @AutoLog(value = "订单主表-添加")
    @ApiOperation(value = "订单主表-添加", notes = "订单主表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody PosOrder posOrder) {
        posOrderService.save(posOrder);
        return Result.ok(posOrderMapper.selectById(posOrder.getId()).getCode());
    }


    /**
     * 添加
     *
     * @param posOrder
     * @return
     */
    @AutoLog(value = "订单提交")
    @ApiOperation(value = "订单提交", notes = "订单提交")
    @PostMapping(value = "/submitOrder")
    public Result<?> submitOrder(@RequestBody @Validated OrderVO posOrder) {
        return posOrderService.submitOrder(posOrder);
    }


    @AutoLog(value = "查询挂单列表")
    @ApiOperation(value = "查询挂单列表", notes = "查询挂单列表")
    @GetMapping(value = "/queryPendingOrder")
    public Result<?> queryPendingOrder(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                       @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        Page page = new Page(pageNo, pageSize);
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Page<Map> data = posOrderService.queryPendingOrder(page, sysUser.getOrgCode());
        return Result.ok(data);
    }

    @AutoLog(value = "解除挂单")
    @ApiOperation(value = "解除挂单", notes = "解除挂单")
    @GetMapping(value = "/endPendingOrder")
    public Result<?> endPendingOrder(@RequestParam String id) {
        return posOrderService.endPendingOrder(id);
    }

    @AutoLog(value = "查询退单")
    @ApiOperation(value = "查询退单", notes = "查询退单")
    @GetMapping(value = "/queryRetractOrder")
    public Result<?> queryRetractOrder(
            @RequestParam(name = "vip") String vip, @RequestParam(name = "orderId") String orderId,
            @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Page page = new Page(pageNo, pageSize);
        HashMap<String, String> searchMap = Maps.newHashMap();
        searchMap.put("vip", vip);
        searchMap.put("orderId", orderId);
        searchMap.put("orgcode", sysUser.getOrgCode());
        Page<Map> data = posOrderService.queryRetractOrder(page, searchMap);
        return Result.ok(data);
    }

    @AutoLog(value = "退货")
    @ApiOperation(value = "退货", notes = "退货")
    @PostMapping(value = "/backOrder")
    public Result<?> backOrder(HttpServletRequest request) {
        String id=request.getParameter("id");
        String count=request.getParameter("count");
        return posOrderService.backOrder(id,new BigDecimal(count));
    }


}
