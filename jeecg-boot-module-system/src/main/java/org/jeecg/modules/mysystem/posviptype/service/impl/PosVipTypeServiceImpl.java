package org.jeecg.modules.mysystem.posviptype.service.impl;

import org.jeecg.modules.mysystem.posviptype.entity.PosVipType;
import org.jeecg.modules.mysystem.posviptype.mapper.PosVipTypeMapper;
import org.jeecg.modules.mysystem.posviptype.service.IPosVipTypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 会员等级
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
@Service
public class PosVipTypeServiceImpl extends ServiceImpl<PosVipTypeMapper, PosVipType> implements IPosVipTypeService {

}
