package org.jeecg.modules.mysystem.posviptopup.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.mysystem.posviptopup.entity.PosVipTopUp;
import org.jeecg.modules.mysystem.posviptopup.mapper.PosVipTopUpMapper;
import org.jeecg.modules.mysystem.posviptopup.service.IPosVipTopUpService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Map;

/**
 * @Description: 会员充值
 * @Author: jeecg-boot
 * @Date:   2021-04-26
 * @Version: V1.0
 */
@Service
public class PosVipTopUpServiceImpl extends ServiceImpl<PosVipTopUpMapper, PosVipTopUp> implements IPosVipTopUpService {


    /**
     * @author: Mr.YSX
     * @description: 会员充值-分页列表查询
     * @date: 2021/4/26 16:20
     * @param page map
     * @return
     */
    @Override
    public IPage<PosVipTopUp> pages(Page<PosVipTopUp> page, Map<String, Object> map) {
        return this.baseMapper.selectPages(page,map);
    }
}
