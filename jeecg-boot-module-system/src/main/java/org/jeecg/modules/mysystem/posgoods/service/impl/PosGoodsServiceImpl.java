package org.jeecg.modules.mysystem.posgoods.service.impl;


import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.api.client.util.Lists;
import com.google.common.collect.Maps;
import com.sun.org.apache.regexp.internal.RE;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mysystem.posgoods.entity.PosGoods;
import org.jeecg.modules.mysystem.posgoods.mapper.PosGoodsMapper;
import org.jeecg.modules.mysystem.posgoods.service.IPosGoodsService;
import org.jeecg.modules.mysystem.posinventory.entity.PosInventory;
import org.jeecg.modules.mysystem.posinventory.mapper.PosInventoryMapper;
import org.jeecg.modules.mysystem.pospurchase.entity.PosPurchase;
import org.jeecg.modules.mysystem.posvipintegral.mapper.PosVipIntegralMapper;
import org.jeecg.modules.mysystem.posvipintegral.service.IPosVipIntegralService;
import org.jeecg.modules.mysystem.posvipuser.entity.PosVipUser;
import org.jeecg.modules.mysystem.posvipuser.mapper.PosVipUserMapper;
import org.jeecg.modules.mysystem.posvipuser.service.IPosVipUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * @Description: 商品表
 * @Author: jeecg-boot
 * @Date: 2021-04-20
 * @Version: V1.0
 */
@Service
@Slf4j
public class PosGoodsServiceImpl extends ServiceImpl<PosGoodsMapper, PosGoods> implements IPosGoodsService {

    @Autowired
    private PosInventoryMapper posInventoryMapper;

    @Autowired
    private PosVipUserMapper posVipUserMapper;

    @Autowired
    private PosVipIntegralMapper posVipIntegralMapper;

    @Autowired
    private IPosVipIntegralService posVipIntegralService;



    @Override
    @Transactional(rollbackFor = Exception.class)
    public void goodsCountsSaveProcess(PosPurchase posPurchase) throws JeecgBootException {
        PosGoods posGoods = baseMapper.selectById(posPurchase.getGoodsId());
        HashMap<String, Object> updateData = Maps.newHashMap();
        //入库管理
        if ("2".equals(posPurchase.getStatus())) {
            updateData.put("id", posGoods.getId());
            updateData.put("count", posPurchase.getCount());
            //使用乐观锁防止并发操作下更新商品库存
            updateData.put("version", posGoods.getVersion());
            int flag = baseMapper.updateGoodsCount(updateData);
            if (flag == 0) {
                //标示有并发操作
                throw new JeecgBootException("有其他用户正在同时修改次商品请稍后再试");
            }
        } else {
            //退货处理
            updateData.put("id", posGoods.getId());
            updateData.put("count", posPurchase.getCount());
            //使用乐观锁防止并发操作下更新商品库存
            updateData.put("version", posGoods.getVersion());
            int flag = baseMapper.backGoodsCounts(updateData);
            if (flag == 0) {
                //标示有并发操作
                throw new JeecgBootException("有其他用户正在同时修改次商品请稍后再试");
            }
            posGoods = baseMapper.selectById(posPurchase.getGoodsId());
            if (posGoods.getCount().compareTo(new BigDecimal(0)) == -1) {
                throw new JeecgBootException("此订单的数量大于商品现有库存");
            }
        }
    }

    @Override
    public Result<?> executeService(String id) {
        //商品重量
        String weight = id;
        //限制门店处于盘点状态下不能使用结算功能
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper queryWrapper = new QueryWrapper<PosInventory>(new PosInventory().setStatus("1").setSysOrgCode(sysUser.getOrgCode()));
        List list = posInventoryMapper.selectList(queryWrapper);
        if (CollUtil.isNotEmpty(list)) {
            return Result.error("后台存在盘点不能使用结算功能");
        }
        //校验是否大华称
        if (id.length() == 18) {
            if (id.length() == 18 && id.startsWith("00")) {
                id = id.substring(2, 7);
                weight = weight.substring(12, 17);
            }
        }
        //查询商品
        QueryWrapper<PosGoods> query = new QueryWrapper<>(new PosGoods().setQrcode(id).setStatus("Y").setDelFlag("1").setSysOrgCode(sysUser.getOrgCode()));
        query.gt("count", "0");
        PosGoods posGoods = baseMapper.selectOne(query);
        if (posGoods == null) {
            return Result.error("商品不存在或者没有库存");
        }
       ;
        posGoods.setWeight(new BigDecimal(weight).multiply(BigDecimal.valueOf(2)).divide(BigDecimal.valueOf(1000), 2, RoundingMode.HALF_UP));
        ArrayList<PosGoods> objects = Lists.newArrayList();
        objects.add(posGoods);
        return Result.OK(new Page<PosGoods>().setRecords(objects));

    }

    /**
     * @author: Mr.YSX
     * @description: pos前台积分兑换商品
     * @date: 2021/5/7 16:14
     * @param   vipCard  会员卡号
     * @param   goodId   商品id
     * @return
     */
    @Override
    @Transactional
    public Result<?> pointDhGoods(String vipCard, String goodId,String number) {
        BigDecimal numbers = new BigDecimal(number);
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String orgCode = sysUser.getOrgCode();
        //查询会员信息
        HashMap<String, Object>  map = new HashMap<String, Object>();
        map.put("vipNumber",vipCard);
        map.put("sysOrgCode",orgCode);
        PosVipUser posVipUser = this.posVipUserMapper.queryVipInfoByVipNumber(map);
        if (ObjectUtils.isEmpty(posVipUser)) {
            return Result.error("会员信息不存在或已被删除!");
        }
        //查询商品信息
        QueryWrapper<PosGoods> posGoodsQueryWrapper = new QueryWrapper<>();
        posGoodsQueryWrapper.eq("id",goodId);
        posGoodsQueryWrapper.eq("del_flag",1);
        posGoodsQueryWrapper.eq("sys_org_code",orgCode);
        posGoodsQueryWrapper.eq("is_points_dh",1);
        PosGoods posGoods = this.baseMapper.selectOne(posGoodsQueryWrapper);
        if (ObjectUtils.isEmpty(posGoods)) {
            return Result.error("商品信息不存在或已被删除!");
        }
        BigDecimal vipIntegral = posVipUser.getVipIntegral();  //会员积分
        BigDecimal dhNeedPoints = posGoods.getDhNeedPoints();  //商品兑换所需积分
        BigDecimal multiply = dhNeedPoints.multiply(numbers);
        if (vipIntegral.compareTo(multiply) < 0) {
            return Result.error("用户积分不足!");
        }
        // 减去用户积分
        BigDecimal subtract = vipIntegral.subtract(multiply);
        int updateUserResult = this.posVipUserMapper.updateById(new PosVipUser()
                .setId(posVipUser.getId())
                .setVipIntegral(subtract)
                .setUpdateTime(new Date()));
        if (updateUserResult == 1) {
            // 进行兑换 增加积分消费记录
            // saveIntegralInfo
            boolean integralInfo = this.posVipIntegralService.saveIntegralInfo(multiply, "2", posVipUser.getId(), "pos积分兑换商品条码 "+posGoods.getQrcode()+"");
            if (!integralInfo){
                throw new JeecgBootException("兑换失败!");
            }
            //消减商品库存
            HashMap<String, Object> updateData = new HashMap<>();
            updateData.put("id",goodId);
            updateData.put("count",numbers);
            updateData.put("version",posGoods.getVersion());
            int i = this.baseMapper.backGoodsCounts(updateData);
            if (i != 1){
                throw new JeecgBootException("兑换失败,消减库存失败!");
            }
            return Result.ok("兑换成功!");
        }
        throw new JeecgBootException("兑换失败!");
    }


}