package org.jeecg.modules.mysystem.posvipbirinfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mysystem.posvipbirinfo.entity.PosVipBirInfo;

/**
 * @Description: 会员生日发送日志
 * @Author: jeecg-boot
 * @Date:   2021-04-27
 * @Version: V1.0
 */
public interface PosVipBirInfoMapper extends BaseMapper<PosVipBirInfo> {

}
