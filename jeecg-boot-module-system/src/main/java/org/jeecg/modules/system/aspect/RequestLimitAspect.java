package org.jeecg.modules.system.aspect;

import com.alibaba.fastjson.JSONObject;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.RequestLimit;
import org.jeecg.common.util.IPUtils;
import org.jeecg.common.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

/**
 * @author yuanxin
 * @Date 2020/12/25 14:19
 **/

@Aspect
@Component
@Order(1)
public class RequestLimitAspect {
    private static final String REQ_LIMIT = "req_limit_";

    @Autowired
    private RedisUtil redis;

    /**
     * 定义拦截规则：拦截com.springboot.bcode.api包下面的所有类中，有@RequestLimit Annotation注解的方法
     * 。
     */
    @Around("@annotation(org.jeecg.common.aspect.annotation.RequestLimit)")
    public Object method(ProceedingJoinPoint pjp) throws Throwable {

        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod(); // 获取被拦截的方法
        RequestLimit limt = method.getAnnotation(RequestLimit.class);
        // No request for limt,continue processing request
        if (limt == null) {
            return pjp.proceed();
        }


        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        int time = limt.time();
        int count = limt.count();
        int waits = limt.waits();

        String ip = IPUtils.getIpAddr(request);
        String url = request.getRequestURI();

        // judge codition
        String key = requestLimitKey(url, ip);

        int nowCount = redis.get(key) == null ? 0 : Integer.valueOf(redis
                .get(key).toString());

        if (nowCount == 0) {
            nowCount++;
            redis.set(key, String.valueOf(nowCount), time);
            return pjp.proceed();
        } else {
            nowCount++;
            redis.set(key, String.valueOf(nowCount));
            if (nowCount >= count) {
                redis.expire(key, waits);
                return returnLimit(request);
            }
        }

        return pjp.proceed();
    }

    /**
     * requestLimitKey: url_ip
     *
     * @param url
     * @param ip
     * @return
     */
    private static String requestLimitKey(String url, String ip) {

        StringBuilder sb = new StringBuilder();
        sb.append(REQ_LIMIT);
        sb.append(url);
        sb.append("_");
        sb.append(ip);
        return sb.toString();
    }

    /**
     * 返回拒绝信息
     *
     * @param request
     * @return
     * @throws IOException
     */
    private String returnLimit(HttpServletRequest request) throws IOException {

        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getResponse();
        PrintWriter out = response.getWriter();
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        Result<Object> errorMes = Result.error("次数请求过于平凡请稍后重试");
        String fasterror = JSONObject.toJSONString(errorMes);
        out.println(fasterror);
        out.flush();
        out.close();
        return null;

    }


}