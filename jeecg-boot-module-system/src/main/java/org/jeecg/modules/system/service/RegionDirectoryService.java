package org.jeecg.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.system.entity.RegionDirectory;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @Author scott
 * @since 2018-12-20
 */
public interface RegionDirectoryService extends IService<RegionDirectory> {


    /**
     * 获取所有的省信息
     *
     * @return java.util.List<java.util.Map>
     */
    List<Map> findCity();

    String convertString(String[] arg);
}
