package org.jeecg.modules.system.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.system.service.RegionDirectoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Title: DuplicateCheckAction
 * @Description: 重复校验工具
 * @Author 张代浩
 * @Date 2019-03-25
 * @Version V1.0
 */
@Slf4j
@RestController
@RequestMapping("/sys/regiondirectory")
@Api(tags="自定义省市区")
public class RegionDirectoryController {


    @Autowired
    private RegionDirectoryService regionDirectoryService;


    @RequestMapping(value = "/allCity", method = RequestMethod.GET)
    @ApiOperation(value = "查询所有省", notes = "查询所有省")
    public Result<?> allCity() {
    List<Map> list= regionDirectoryService.findCity();
        return Result.ok(list);
    }
}
