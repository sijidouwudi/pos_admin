package org.jeecg.modules.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.system.entity.RegionDirectory;
import org.jeecg.modules.system.mapper.RegionDirectoryMapper;
import org.jeecg.modules.system.service.RegionDirectoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * excel导入 实现类
 */
@Slf4j
@Service
public class RegionDirectoryServiceImpl extends ServiceImpl<RegionDirectoryMapper, RegionDirectory> implements RegionDirectoryService {


    @Resource
    private RedisUtil redisUtil;

    /**
     * 缓存了所有的省市区信息
     *
     * @return java.util.Map<java.lang.Integer, java.lang.String>
     */
    public Map<Integer, String> getCity() {
        List<List<RegionDirectory>> regionDirectories = new ArrayList<>();
        Object city = redisUtil.lGet("city", 0, -1);
        if (ObjectUtil.isEmpty(city)) {
            List<RegionDirectory> pid = this.baseMapper.selectList(new QueryWrapper<>(new RegionDirectory()).isNotNull("pid"));
            redisUtil.lSet("city", pid);
            regionDirectories.add(pid);
        } else {
            regionDirectories = (List<List<RegionDirectory>>) city;
        }
        List<RegionDirectory> regionDirectories1 = regionDirectories.get(0);
        log.info("我的数组长度是{}", regionDirectories1.size());
        return regionDirectories1.stream().collect(Collectors.toMap(RegionDirectory::getId, RegionDirectory::getName));
    }

    @Override
    public List<Map> findCity() {
        return this.baseMapper.findOnleCity();
    }

    @Override
    public String convertString(String[] data) {
        StringBuffer stringBuffer = new StringBuffer();
        Map<Integer, String> city = getCity();
        for (int i = 0; i < data.length; i++) {
            int i1 = Integer.parseInt(data[i]);
            String s = city.get(i1);
            stringBuffer.append(s);
            if (i + 1 != data.length) {
                stringBuffer.append("/");
            }
        }
        return stringBuffer.toString();
    }
}
