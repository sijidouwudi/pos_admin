package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.system.entity.RegionDirectory;

import java.util.List;
import java.util.Map;

/**
 * @Description: 系统通告表
 * @Author: jeecg-boot
 * @Date:  2019-01-02
 * @Version: V1.0
 */
public interface RegionDirectoryMapper extends BaseMapper<RegionDirectory> {

    @Select("select id,name from region_directory where pid=1")
    List<Map> findOnleCity();
}
