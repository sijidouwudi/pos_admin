package org.jeecg.modules.app.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.modules.mysystem.couser.mapper.CoUserMapper;
import org.jeecg.modules.app.MonitorRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yuanxin
 * @Date 2020/9/2 17:11
 **/
@Slf4j
@RestController
@RequestMapping("/app/user")
@Api(tags = "用户管理")
public class AppUserController {

    @Autowired
    private CoUserMapper coUserMapper;

    @AutoLog(value = "获取用户信息")
    @ApiOperation(value = "获取用户信息", notes = "获取用户信息")
    @PostMapping(value = "/userInfo")
    @MonitorRequest
    public Result<?> userInfo() {
        return Result.ok(coUserMapper.getUserByNumber(JwtUtil.getAppUsername()));
    }
}