package org.jeecg.modules.app.controller;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.modules.app.factory.SmsFactory;
import org.jeecg.modules.app.factory.factoryhandler.SmsHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author yuanxin
 * @Date 2020/9/2 14:39
 **/
@RestController
@RequestMapping("/app/sms")
@Slf4j
public class SendSmsController {


    /**
     * 登录发送验证码
     *
     * @param maps 手机号
     * @return org.jeecg.common.api.vo.Result<java.lang.Object>
     */
    @AutoLog(value = "登录发送验证码")
    @ApiOperation(value = "登录发送验证码", notes = "登录发送验证码")
    @PostMapping("/sendSMS")
    public Result<?> sendSMS(@RequestBody Map<String, String> maps) {
        String phone = maps.get("phone");
        if (StrUtil.isBlank(phone) || !Validator.isMobile(phone)) {
            return Result.error("手机号不能为空或者手机号格式不正确");
        }
        SmsHandler login = SmsFactory.getInvokHandler(maps.get("flag").toString());
        return login.Handler(phone);

    }

}