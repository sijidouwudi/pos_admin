package org.jeecg.modules.app.factory;

import org.jeecg.modules.app.factory.factoryhandler.SmsHandler;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yuanxin
 * @Date 2020/9/2 17:22
 **/
@Component
public class SmsFactory {

    private static Map<String, SmsHandler> map = new HashMap<>();

    public static SmsHandler getInvokHandler(String name) {
        return map.get(name);
    }


    public static void registory(String name, SmsHandler smsHandler) {
        map.put(name, smsHandler);
    }
}