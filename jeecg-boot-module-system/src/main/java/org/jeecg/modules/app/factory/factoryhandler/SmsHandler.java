package org.jeecg.modules.app.factory.factoryhandler;

import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.InitializingBean;

/**
 * @author yuanxin
 * @Date 2020/9/2 17:25
 **/
public interface SmsHandler extends InitializingBean {

    Result<?> Handler(String phone);
}