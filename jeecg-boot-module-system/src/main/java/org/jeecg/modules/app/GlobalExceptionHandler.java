package org.jeecg.modules.app;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author yuanxin
 * @Date 2020/6/12 0:27
 **/
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {


    /**
     * 处理自定义异常
     */
    @ExceptionHandler(value = BusinessException.class)
    public Result<?> badRequestException(BusinessException e) {
        if(e.getMessage().equals("请去登录")){
            return Result.error(10000,"用户会话过期");
        }
        return Result.error(e.getMessage());
    }
}