package org.jeecg.modules.app;

import java.lang.annotation.*;

//次注解用于移动端的token校验
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MonitorRequest {
}
