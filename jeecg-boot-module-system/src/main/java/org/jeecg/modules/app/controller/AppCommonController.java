package org.jeecg.modules.app.controller;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.PasswordUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mysystem.couser.entity.CoUser;
import org.jeecg.modules.mysystem.couser.mapper.CoUserMapper;
import org.jeecg.modules.app.vo.LoginVO;
import org.jeecg.modules.app.vo.RegisterVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;
import java.util.Set;

/**
 * @author yuanxin
 * @Date 2020/9/1 15:32
 **/

@Slf4j
@RestController
@RequestMapping("/app/common")
@Api(tags = "移动端通用代码")
public class AppCommonController {
    @Autowired
    private CoUserMapper coUserMapper;

    @Autowired
    private RedisUtil redisUtil;



    @AutoLog(value = "app注册")
    @ApiOperation(value = "app注册", notes = "app注册")
    @PostMapping(value = "/register")
    public Result<?> register(@RequestBody @Validated RegisterVO registerVO) {
        CoUser coUser = coUserMapper.getUserByNumber(registerVO.getAccountNumber());
        if (coUser != null) {
            return Result.error("手机号已经注册请直接登录!");
        }
        String codes = (String) redisUtil.get(registerVO.getAccountNumber());
        if (StrUtil.isEmpty(codes)) {
            return Result.error("请先发送验证码!");
        }
        if (!codes.equals(registerVO.getCode())) {
            return Result.error("手机验证码错误!");
        }
        //注册成功后主动删除验证码
        redisUtil.del(registerVO.getAccountNumber());
        //封装app用户信息入库
        //保存用户
        int flag = savePerson(registerVO);
        if (flag == 1) {
            //保存成功返回token给app
            String token = JwtUtil.sign(registerVO.getAccountNumber(), registerVO.getPassword());
            // 设置token缓存有效时间
            redisUtil.set(CommonConstant.PREFIX_USER_APP_TOKEN +registerVO.getAccountNumber()+token,token, ((JwtUtil.EXPIRE_TIME * 2 / 1000) * 48) * 15);
            return Result.ok(token);
        } else {
            return Result.error("密码保存失败");
        }
    }


    @AutoLog(value = "app登录")
    @ApiOperation(value = "app登录", notes = "app登录")
    @PostMapping(value = "/login")
    public Result<?> login(@RequestBody LoginVO loginVO) {
        //检查用户是否存在或者已被冻结
        CoUser userByNumber = coUserMapper.getUserByNumber(loginVO.getAccountNumber());
        if (userByNumber == null || userByNumber.getStatus().intValue() == 2) {
            return Result.error("用户不存在或者已被冻结!");
        }
        String userpassword = PasswordUtil.encrypt(loginVO.getAccountNumber(), loginVO.getPassword(), userByNumber.getSalt());
        if (!userByNumber.getPassword().equals(userpassword)) {
            return Result.error("用户密码错误!");
        }
        //删除其他设备登录的用户
        Set keys = redisUtil.keys(CommonConstant.PREFIX_USER_APP_TOKEN+loginVO.getAccountNumber());
        Iterator<String> it = keys.iterator();
        while (it.hasNext()) {
            redisUtil.del(it.next());
        }
        //保存成功返回token给app
        String token = JwtUtil.sign(loginVO.getAccountNumber(), userpassword);
        // 设置token缓存有效时间
        redisUtil.set(CommonConstant.PREFIX_USER_APP_TOKEN +loginVO.getAccountNumber()+token, token,((JwtUtil.EXPIRE_TIME * 2 / 1000) * 48) * 15);
        //保证当前用户只有一个登录
        return Result.ok(token);
    }


    @AutoLog(value = "忘记密码")
    @ApiOperation(value = "忘记密码", notes = "忘记密码")
    @PostMapping(value = "/updPass")
    public Result<?> updPass(@RequestBody @Validated RegisterVO registerVO) {
        CoUser coUser = coUserMapper.getUserByNumber(registerVO.getAccountNumber());
        if (coUser == null) {
            return Result.error("账号不存在");
        }
        String codes = (String) redisUtil.get(coUser.getNumber());
        if (StrUtil.isEmpty(codes)) {
            return Result.error("请先发送验证码!");
        }
        if (!codes.equals(registerVO.getCode())) {
            return Result.error("手机验证码错误!");
        }
        //注册成功后主动删除验证码
        redisUtil.del(coUser.getNumber());
        //修改密码
        int flag = coUserMapper.updateById(coUser.setPassword(PasswordUtil.encrypt(registerVO.getAccountNumber(), registerVO.getPassword(), coUser.getSalt())));
        if (flag == 1) {
            //删除其他设备登录的用户
            Set keys = redisUtil.keys(CommonConstant.PREFIX_USER_APP_TOKEN+registerVO.getAccountNumber());
            Iterator<String> it = keys.iterator();
            while (it.hasNext()) {
                redisUtil.del(it.next());
            }
            //保存成功返回token给app
            String token = JwtUtil.sign(coUser.getNumber(), coUser.getPassword());
            // 设置token缓存有效时间
            redisUtil.set(CommonConstant.PREFIX_USER_APP_TOKEN +registerVO.getAccountNumber()+token, token,((JwtUtil.EXPIRE_TIME * 2 / 1000) * 48) * 15);
            return Result.ok(token);
        } else {
            return Result.error("密码修改失败");
        }
    }


    /**
     * 保存用户数据
     * @param registerVO
     * @return int
     */
    private int savePerson(RegisterVO registerVO) {
        //盐
        String saft = oConvertUtils.randomGen(8);
        CoUser coUser=new CoUser();
        coUser.setNumber(registerVO.getAccountNumber());
        String passwordEncode = PasswordUtil.encrypt(registerVO.getAccountNumber(), registerVO.getPassword(), saft);
        coUser.setPassword(passwordEncode);
        coUser.setSalt(saft);
       return  coUserMapper.insert(coUser);
    }
}