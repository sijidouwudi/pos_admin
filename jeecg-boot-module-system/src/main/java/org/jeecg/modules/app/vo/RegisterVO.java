package org.jeecg.modules.app.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author yuanxin
 * @Date 2020/9/2 10:44
 **/
@Data
public class RegisterVO {

    @NotBlank(message = "账号不能为空")
    private String accountNumber;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotBlank(message = "验证码不能为空")
    private String code;
}