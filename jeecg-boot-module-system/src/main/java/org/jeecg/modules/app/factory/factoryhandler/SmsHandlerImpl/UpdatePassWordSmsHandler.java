package org.jeecg.modules.app.factory.factoryhandler.SmsHandlerImpl;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.DySmsEnum;
import org.jeecg.common.util.DySmsHelper;
import org.jeecg.common.util.RedisUtil;

import org.jeecg.modules.app.factory.SmsFactory;
import org.jeecg.modules.app.factory.factoryhandler.SmsHandler;
import org.jeecg.modules.mysystem.couser.entity.CoUser;
import org.jeecg.modules.mysystem.couser.mapper.CoUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author yuanxin
 * @Date 2020/9/2 17:29
 **/
@Slf4j
@Component
public class UpdatePassWordSmsHandler implements SmsHandler {
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private CoUserMapper coUserMapper;


    @Override
    public Result<?> Handler(String phone) {
        //查询该手机号是否已经被绑定
        CoUser userByNumber = coUserMapper.getUserByNumber(phone);
        if (userByNumber == null) {
            return Result.error("该用户不存在");
        }
        String baseCode = (String) redisUtil.get(phone);
        if (!StringUtils.isBlank(baseCode)) {
            return Result.error("三分钟只能发送一次,请勿频繁操作");
        }
        //生成6位数验证码
        String code = RandomUtil.randomNumbers(6);
        JSONObject obj = new JSONObject();
        obj.put("code", code);
        //发送短信
        try {
            boolean b = DySmsHelper.sendSms(phone, obj, DySmsEnum.REGISTER_TEMPLATE_CODE);
            if (b == false) {
                return Result.error("短信验证码发送失败,请稍后重试");
            }
        } catch (ClientException e) {
            e.printStackTrace();
            return Result.error("短信接口未系配置,请联管理员!");
        }
        log.info(code);
        //短信有效时间180秒
        redisUtil.set(phone, code, 180);
        return Result.ok("短信发送成功");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        SmsFactory.registory("password", this);
    }
}