package org.jeecg.modules.app.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author yuanxin
 * @Date 2020/9/2 10:19
 **/
@Data
public class LoginVO {


    @NotBlank(message = "账号不能为空")
    private String accountNumber;

    @NotBlank(message = "密码不能为空")
    private String password;


}