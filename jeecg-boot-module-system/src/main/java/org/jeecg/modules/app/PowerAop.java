package org.jeecg.modules.app;

import cn.hutool.core.util.StrUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * 用来对app的所有请求进行权限拦截
 *
 * @author yuanxin
 * @Date 2020/6/11 sd23:09
 **/
@Aspect
@Component
public class PowerAop {

    //从redis获取token进行校验
    @Autowired
    private RedisUtil redisUtil;


    @Before(value = "@annotation(org.jeecg.modules.app.MonitorRequest)")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
            //校验token
            String header = request.getHeader("X-Access-Token");
            //如果为空提示去登录
            if (StrUtil.isEmpty(header)) {
                throw new BusinessException("请去登录");
            }
            if (Appauth(header)) {

            } else {
                throw new BusinessException("请去登录");
            }

    }


    //校验token的有效性
    public boolean Appauth(String header) {
        String username = JwtUtil.getUsername(header);
        if (StrUtil.isEmpty(username)) {
            return false;
        }
        String token = (String) redisUtil.get(CommonConstant.PREFIX_USER_APP_TOKEN +username+ header);
        if (StrUtil.isEmpty(token)) {
            return false;
        }
        return true;
    }


}